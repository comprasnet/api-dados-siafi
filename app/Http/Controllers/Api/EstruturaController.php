<?php

namespace App\Http\Controllers\Api;

use App\Models\Naturezasubitens;
use App\Models\Orgao;
use App\Models\Unidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EstruturaController extends Controller
{

    /**
     * @OA\Get(
     *     path="/api/estrutura/orgaossuperiores",
     *     tags={"Órgãos"},
     *     summary="Retorna todos os órgãos superiores.",
     *     description="Retorna todos os órgãos superiores existentes na base.",
     *     security={ {"bearerAuth": {} }},
     *     @OA\Response(
     *         response=200,
     *         description="Órgãos superiores",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/orgaossuperiores")
     *         ),
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Error: Unauthorized",
     *          @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *     ),
     * )
     *
     */
    public function buscaOrgaosSuperiores()
    {
        $orgaos_superiores = Orgao::where('orgao_superior','')
            ->orWhereNull('orgao_superior')
            ->orderBy('codigo')
            ->get();

        return  response()->json($orgaos_superiores);
    }

    /**
     * @OA\Get(
     *     path="/api/estrutura/orgaos",
     *     tags={"Órgãos"},
     *     summary="Retorna todos os órgãos.",
     *     description="Retorna todos os órgãos existentes na base.",
     *     security={ {"bearerAuth": {} }},
     *     @OA\Response(
     *         response=200,
     *         description="Órgãos",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/orgaos")
     *         ),
     *     ),
     *      @OA\Response(
     *           response=401,
     *           description="Error: Unauthorized",
     *           @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *      ),
     * )
     *
     */
    /**
     * Retorna todos os órgãos existentes na base
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function buscaOrgaos()
    {
        $orgaos = new Orgao();

        return  response()->json($orgaos->buscaTodosOrgaos());
    }

    /**
     * @OA\Get(
     *     path="/api/estrutura/unidades",
     *     tags={"Unidades"},
     *     summary="Retorna as unidades cuja função é 1 (Executora) ou 3 (Controle).",
     *     security={ {"bearerAuth": {} }},
     *     @OA\Response(
     *         response=200,
     *         description="Unidades",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/unidades")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error: Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *     ),
     * )
     */

    /**
     * Retorna as Unidades existentes na base cuja 'funcao' é 1 ou 3
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function buscaUnidades()
    {
        $unidades = new Unidade();

        return  response()->json($unidades->buscaUnidade());

    }


    /**
     * @OA\Get(
     *     path="/api/estrutura/naturezadespesas",
     *     tags={"Natureza de Despesa"},
     *     summary="Retorna as naturezas de despesas.",
     *     description="Retorna todas as naturezas de despesa disponíveis.",
     *     @OA\Response(
     *         response=200,
     *         description="Operação bem-sucedida",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/NaturezasDespesa")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error: Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *     ),
     * )
     */
    public function buscaNaturezadespesas()
    {
        $naturezadespesa = new Naturezasubitens();

        return response()->json($naturezadespesa->buscaNaturezaDespesas());

    }
}
