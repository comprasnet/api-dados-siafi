<?php

namespace App\Http\Controllers\Api;

use App\Models\Credor;
use App\Models\Empenhodetalhado;
use App\Models\Naturezasubitens;
use App\Models\Obxne;
use App\Models\Ordembancaria;
use App\Models\Planointerno;
use App\Models\Rp;
use App\Models\Unidade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Empenho;


class RpController extends BaseController
{
    public function ler()
    {
        $nomearquivo = $this->buscaArquivos();

        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);

        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if (substr($nome['nomearquivo'], 0, 2) == 'rp' or substr($nome['nomearquivo'], 6, 2) == 'rp') {
                    $empenho = $this->lerArquivo($nome['nomearquivo'], $nome['case']);


//                    if (substr($nome['nomearquivo'], 2, 4) == '2020' or substr($nome['nomearquivo'], 8, 4) == '2020') {
//                        $ano =
//                    }else{
//                        $ano = date('Y');
//                    }

                    foreach ($empenho as $e) {
                        $busca = $this->buscaRp($e['ug'], $e['gestao'], $e['numero']);
                        $empenho = $this->buscaEmpenho($e['ug'], $e['gestao'], $e['numero']);
                        if (!isset($busca->numero)) {
                            $novo_empenho = new Rp;
                            $novo_empenho->ug = $e['ug'];
                            $novo_empenho->gestao = $e['gestao'];
                            $novo_empenho->numero = $e['numero'];
                            $novo_empenho->numero_ref = $e['numero_ref'];
                            $novo_empenho->emissao = $e['emissao'];
                            $novo_empenho->tipofavorecido = $e['tipofavorecido'];
                            $novo_empenho->favorecido = $e['favorecido'];
                            $novo_empenho->observacao = $e['observacao'];
                            $novo_empenho->fonte = $e['fonte'];
                            $novo_empenho->naturezadespesa = $e['naturezadespesa'];
                            $novo_empenho->planointerno = $e['planointerno'];
                            $novo_empenho->num_lista = $e['num_lista'];
                            $novo_empenho->save();

                            if (!isset($empenho->numero)) {
                                $empenho->rp = true;
                                $empenho->save();
                            }

                        } else {
                            if (!isset($empenho->numero)) {
                                $empenho->rp = true;
                                $empenho->save();
                            }
                        }
                    }
                }

            }

            $ok = 'RP´s lidos.';

        } else {
            $ok = 'Não Há arquivos para leitura.';
        }

        return $ok;

    }

    protected function buscaRp($ug, $gestao, $numero)
    {

        $rp = Rp::where('ug', $ug)
            ->where('gestao', $gestao)
            ->where('numero', $numero)
            ->first();

        return $rp;

    }

    protected function buscaEmpenho($ug, $gestao, $numero)
    {

        $empenho = Empenho::where('ug', $ug)
            ->where('gestao', $gestao)
            ->where('numero', $numero)
            ->first();

        return $empenho;

    }

    public function lerArquivo($nomeaquivo, $case)
    {
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $name = $path . $nomeaquivo;
        $namedestino = $path_processados . $nomeaquivo;

        if ($case == 0) {
            $extref = ".REF.gz";
            $exttxt = ".TXT.gz";
        }

        if ($case == 1) {
            $extref = ".ref.gz";
            $exttxt = ".txt.gz";
        }

        $myfileref = gzopen($name . $extref, "r") or die("Unable to open file!");

        $i = 0;
        while (!gzeof($myfileref)) {
            $line = gzgets($myfileref);

            if (strlen($line) == 0) {
                break;
            }

            $ref[$i]['column'] = trim(substr($line, 0, 40));
            $ref[$i]['type'] = trim(substr($line, 40, 1));

            if (strstr(trim(substr($line, 42, 4)), ",") != false) {
                $num = explode(",", trim(substr($line, 42, 4)));
                $ref[$i]['size'] = $num[0] + $num[1];
                $ref[$i]['decimal'] = $num[1];
            } else {
                $ref[$i]['size'] = trim(substr($line, 42, 4)) * 1;
                $ref[$i]['decimal'] = 0;
            }

            $ref[$i]['acu'] = ($i == 0) ? $ref[$i]['size'] : $ref[$i]['size'] + $ref[$i - 1]['acu'];
            $i++;
        }
        $NUMCOLS = $i;
        gzclose($myfileref);

        $myfiletxt = gzopen($name . $exttxt, "r") or die("Unable to open file!");
        $i = 0;
        $j = 0;
        while (!gzeof($myfiletxt)) {
            $line = gzgets($myfiletxt);
            for ($j = 0; $j < $NUMCOLS; $j++) {
                $campo = $ref[$j]['column'];
                $inicio = ($j == 0) ? 0 : $ref[$j - 1]['acu'];
                $valor = trim(substr($line, $inicio, $ref[$j]['size']));
                if ($ref[$j]['type'] == "N") {
                    $valor = $valor * pow(10, -$ref[$j]['decimal']);
                }
                if ($campo == 'GR-UG-GESTAO-AN-NUMERO-NEUQ(1)') {
                    $empenho[$i]['ug'] = substr($valor, 0, 6);
                    $empenho[$i]['gestao'] = substr($valor, 6, 5);
                    $empenho[$i]['numero'] = substr($valor, 11, 12);
                }
                if ($campo == 'GR-AN-NU-DOCUMENTO-REFERENCIA') {
                    $empenho[$i]['numero_ref'] = $valor;
                }
                if ($campo == 'IT-DA-EMISSAO') {
                    $empenho[$i]['emissao'] = substr($valor, 0, 4) . '-' . substr($valor, 4, 2) . '-' . substr($valor,
                            6, 2);
                }
                if ($campo == 'IT-IN-FAVORECIDO') {
                    $empenho[$i]['tipofavorecido'] = $valor;
                }
                if ($campo == 'IT-CO-FAVORECIDO') {
                    if ($empenho[$i]['tipofavorecido'] == 4) {
                        $empenho[$i]['favorecido'] = substr($valor, 0, 6);
                    } else {
                        $empenho[$i]['favorecido'] = $valor;
                    }
                }
                if ($campo == 'IT-TX-OBSERVACAO') {
                    $empenho[$i]['observacao'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'GR-FONTE-RECURSO') {
                    $empenho[$i]['fonte'] = $valor;
                }
                if ($campo == 'GR-NATUREZA-DESPESA') {
                    $empenho[$i]['naturezadespesa'] = $valor;
                }
                if ($campo == 'IT-CO-PLANO-INTERNO') {
                    $empenho[$i]['planointerno'] = $valor;
                }
                if ($campo == 'IT-NU-LISTA(1)') {
                    $empenho[$i]['num_lista'] = $valor;
                }
            }

            if ($empenho[$i]['fonte'] == '' and $empenho[$i]['naturezadespesa'] == '0') {
                unset($empenho[$i]);
            }
            $i++;
        }
        gzclose($myfiletxt);

        $this->moverArquivoProcessado($namedestino, $name, $extref);

        $this->moverArquivoProcessado($namedestino, $name, $exttxt);


        return $empenho;
    }

    public function buscaRpPorUgold($ano, $ug)
    {
        $data = $ano . '-01-01';

        $empenhos = Empenho::
        with(
            array('itens' => function ($query) use ($ano) {

                $query->select(
                    'empenhodetalhado.numeroli',
                    'empenhodetalhado.numitem',
                    'empenhodetalhado.subitem',
                    DB::raw('naturezasubitens.descricao_subitem as subitemdescricao'),
                    DB::raw("case
                                         when (sum(empenhodetalhado.valortotal) = 0 OR sum(empenhodetalhado.quantidade) = 0)
                                            then
                                            round(sum(empenhodetalhado.valorunitario) / count(empenhodetalhado.numeroli),2)
                                        else
                                            round(sum(empenhodetalhado.valortotal) / sum(empenhodetalhado.quantidade),2)
                                        end as valorunitario
                                    "
                    ),
                    DB::raw("sum(empenhodetalhado.quantidade) as quantidade"),
                    DB::raw("sum(empenhodetalhado.valortotal) as valortotal")
                )
                    ->join('empenhos', function ($join) {
                        $join->on('empenhos.num_lista', '=', 'empenhodetalhado.numeroli')
                            ->on('empenhos.ano_emissao', '=', 'empenhodetalhado.ano_emissao');
                    })
                    ->join('naturezasubitens', function ($join) {
                        $join->on('naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
                            ->on('naturezasubitens.codigo_subitem', '=', 'empenhodetalhado.subitem');
                    })
                    ->groupBy(
                        'empenhodetalhado.numeroli',
                        'empenhodetalhado.numitem',
                        'empenhodetalhado.subitem',
                        'naturezasubitens.descricao_subitem'
                    )
                    ->where('empenhodetalhado.ano_emissao', DB::raw($ano));
            })
        )
            ->select(['empenhos.num_lista', 'empenhos.ug', 'empenhos.gestao', 'empenhos.numero', 'empenhos.emissao'
                , 'empenhos.tipofavorecido as tipocredor'
                , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
                , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
                , 'empenhos.observacao'
                , 'empenhos.fonte'
                , 'empenhos.modalidade_licitacao_siafi'
                , 'empenhos.ptres'
                , 'empenhos.naturezadespesa'
                , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
                , 'empenhos.planointerno as picodigo'
                , 'planointerno.descricao as pidescricao'
                , 'empenhos.sistemaorigem'
                , 'empenhos.informacaocomplementar'
            ])
            ->distinct()
            ->leftJoin('naturezasubitens', 'naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
            ->leftJoin('planointerno', 'planointerno.codigo', '=', 'empenhos.planointerno')
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor', DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"), '=', 'empenhos.favorecido')
            ->where('empenhos.ug', $ug)
            ->where('empenhos.updated_at', '>=', $data)
            ->where('empenhos.ano_emissao', $ano)
            ->where(DB::raw('left(empenhos."numero", 4)'), '<>', $ano)
            ->get();

        return response()->json($empenhos, 200);

    }

    public function buscaRpPorDiaUg($ano, $ug, $data = null)
    {

        if (!$data) {
            $data = date('Y-m-d');
        }
        $dataMenosCinco = Carbon::parse($data)->subDay(5);

        $empenhos = Empenho::
        with(
            array('itens' => function ($query) use ($ano) {

                $query->select(
                    'empenhodetalhado.numeroli',
                    'empenhodetalhado.numitem',
                    'empenhodetalhado.subitem',
                    DB::raw('naturezasubitens.descricao_subitem as subitemdescricao'),
                    DB::raw("case
                                         when (sum(empenhodetalhado.valortotal) = 0 OR sum(empenhodetalhado.quantidade) = 0)
                                            then
                                            round(sum(empenhodetalhado.valorunitario) / count(empenhodetalhado.numeroli),2)
                                        else
                                            round(sum(empenhodetalhado.valortotal) / sum(empenhodetalhado.quantidade),2)
                                        end as valorunitario
                                    "
                    ),
                    DB::raw("sum(empenhodetalhado.quantidade) as quantidade"),
                    DB::raw("sum(empenhodetalhado.valortotal) as valortotal")
                )
                    ->join('empenhos', function ($join) {
                        $join->on('empenhos.num_lista', '=', 'empenhodetalhado.numeroli')
                            ->on('empenhos.ano_emissao', '=', 'empenhodetalhado.ano_emissao');
                    })
                    ->join('naturezasubitens', function ($join) {
                        $join->on('naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
                            ->on('naturezasubitens.codigo_subitem', '=', 'empenhodetalhado.subitem');
                    })
                    ->groupBy(
                        'empenhodetalhado.numeroli',
                        'empenhodetalhado.numitem',
                        'empenhodetalhado.subitem',
                        'naturezasubitens.descricao_subitem'
                    )
                    ->where('empenhodetalhado.ano_emissao', DB::raw($ano));
            })
        )
            ->select(['empenhos.num_lista', 'empenhos.ug', 'empenhos.gestao', 'empenhos.numero', 'empenhos.emissao', 'ano_emissao'
                , 'empenhos.tipofavorecido as tipocredor'
                , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
                , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
                , 'empenhos.observacao'
                , 'empenhos.fonte'
                , 'empenhos.modalidade_licitacao_siafi'
                , 'empenhos.ptres'
                , 'empenhos.naturezadespesa'
                , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
                , 'empenhos.planointerno as picodigo'
                , 'planointerno.descricao as pidescricao'
                , 'empenhos.sistemaorigem'
                , 'empenhos.informacaocomplementar'
            ])
            ->distinct()
            ->leftJoin('naturezasubitens', 'naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
            ->leftJoin('planointerno', 'planointerno.codigo', '=', 'empenhos.planointerno')
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor', DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"), '=', 'empenhos.favorecido')
            ->where('empenhos.ug', $ug)
            ->where('empenhos.updated_at', '>=', $dataMenosCinco)
            ->where('empenhos.ano_emissao', $ano)
            ->where(DB::raw('left(empenhos."numero", 4)'), '<>', $ano)
            ->get();

        return response()->json($empenhos, 200);

    }

    /**
     * @OA\Get(
     *      path="/api/rp/ano/{ano}/ug/{ug}",
     *      operationId="buscaRpPorUg",
     *      tags={"Empenhos"},
     *      summary="Retorna os restos a pagar por ano e unidade gestora.",
     *      description="Retorna os empenhos de restos a pagar e itens de um ano e uma unidade gestora,
                         cuja atualização (updated_at) vai desde a data fornecida até cinco dias anteriores.<br>
                         Também filtra os empenhos emitidos no ano fornecido
                         e que comecem com este ano, seguido de 'NE'.
     *                   Ex.: 2023NE*",
     *      @OA\Parameter(
     *          name="ano",
     *          in="path",
     *          required=true,
     *          description="Ano do empenho",
     *          example="2024",
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *          name="ug",
     *          in="path",
     *          required=true,
     *          description="Unidade gestora",
     *          example="123456",
     *          @OA\Schema(type="integer")
     *      ),     *
     *      @OA\Response(
     *          response=200,
     *          description="Operação bem-sucedida",
     *          @OA\JsonContent(ref="#/components/schemas/ArrayDeEmpenhosComItens")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Error: Unauthorized",
     *          @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *      ),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     * )
     */
    public function buscaRpPorUg($ano, $ug, $data = null)
    {

        if (!$data) {
            $dataMenosCinco = $ano . '-01-01';
        }else{
            $dataMenosCinco = Carbon::parse($data)->subDay(5);
        }


        $empenhos = Empenho::select(['empenhos.num_lista', 'empenhos.ug', 'empenhos.gestao', 'empenhos.numero', 'empenhos.emissao', 'ano_emissao'
            , 'empenhos.tipofavorecido as tipocredor'
            , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
            , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
            , 'empenhos.observacao'
            , 'empenhos.fonte'
            , 'empenhos.modalidade_licitacao_siafi'
            , 'empenhos.ptres'
            , 'empenhos.naturezadespesa'
            , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
            , 'naturezasubitens.sistema_origem as nd_sistema_origem'
            , 'empenhos.planointerno as picodigo'
            , 'planointerno.descricao as pidescricao'
            , 'planointerno.sistema_origem as pi_sistema_origem'
            , 'empenhos.sistemaorigem'
            , 'empenhos.id_sistema_origem'
            , 'empenhos.informacaocomplementar'
            , 'empenhos.ano_emissao'
        ])
            ->distinct()
            ->leftJoin('naturezasubitens', 'naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
            ->leftJoin('planointerno', 'planointerno.codigo', '=', 'empenhos.planointerno')
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor', DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"), '=', 'empenhos.favorecido')
            ->where('empenhos.ug', $ug)
            ->where('empenhos.updated_at', '>=', $dataMenosCinco)
            ->where('empenhos.ano_emissao', $ano)
            ->where(DB::raw('left(empenhos."numero", 4)'), '<>', $ano)
            ->get();

        $retorno = [];
        foreach ($empenhos as $empenho) {
            $retorno[] = [
                "num_lista" => $empenho->num_lista,
                "ug" => $empenho->ug,
                "gestao" => $empenho->gestao,
                "numero" => $empenho->numero,
                "emissao" => $empenho->emissao,
                "ano_emissao" => $empenho->ano_emissao,
                "tipocredor" => $empenho->tipocredor,
                "cpfcnpjugidgener" => $empenho->cpfcnpjugidgener,
                "nome" => $empenho->nome,
                "observacao" => $empenho->observacao,
                "fonte" => $empenho->fonte,
                "modalidade_licitacao_siafi" => $empenho->modalidade_licitacao_siafi,
                "ptres" => $empenho->ptres,
                "naturezadespesa" => $empenho->naturezadespesa,
                "naturezadespesadescricao" => $empenho->naturezadespesadescricao,
                "nd_sistema_origem" => $empenho->nd_sistema_origem,
                "picodigo" => $empenho->picodigo,
                "pidescricao" => $empenho->pidescricao,
                "pi_sistema_origem" => $empenho->pi_sistema_origem,
                "sistemaorigem" => $empenho->sistemaorigem,
                "id_sistema_origem" => $empenho->id_sistema_origem,
                "informacaocomplementar" => $empenho->informacaocomplementar,
                "itens" => $this->retornaItensRp($empenho, $ano),
            ];

        }

        return response()->json($retorno, 200);

    }

    public function retornaItensRp($empenho, $ano)
    {
        $empenhodetalhado = Empenhodetalhado::select(
            'empenhodetalhado.numeroli',
            'empenhodetalhado.numitem',
            'empenhodetalhado.subitem',
            DB::raw('naturezasubitens.descricao_subitem as subitemdescricao'),
            DB::raw("case
                                         when (sum(empenhodetalhado.valortotal) = 0 OR sum(empenhodetalhado.quantidade) = 0)
                                            then
                                            round(sum(empenhodetalhado.valorunitario) / count(empenhodetalhado.numeroli),2)
                                        else
                                            round(sum(empenhodetalhado.valortotal) / sum(empenhodetalhado.quantidade),2)
                                        end as valorunitario
                                    "
            ),
            DB::raw("sum(empenhodetalhado.quantidade) as quantidade"),
            DB::raw("sum(empenhodetalhado.valortotal) as valortotal")
        )
            ->join('naturezasubitens', function ($join) {
                $join->on('naturezasubitens.codigo_subitem', '=', 'empenhodetalhado.subitem');
            })
            ->groupBy(
                'empenhodetalhado.numeroli',
                'empenhodetalhado.numitem',
                'empenhodetalhado.subitem',
                'naturezasubitens.descricao_subitem'
            )
            ->where('naturezasubitens.codigo_nd', $empenho->naturezadespesa)
            ->where('empenhodetalhado.numeroli', $empenho->num_lista)
            ->where('empenhodetalhado.ano_emissao', DB::raw($ano))
            ->get();

        return $empenhodetalhado->toArray();

    }

}
