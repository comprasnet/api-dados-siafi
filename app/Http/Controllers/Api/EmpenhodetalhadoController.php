<?php

namespace App\Http\Controllers\Api;

use App\Models\Empenho;
use App\Models\Empenhodetalhado;
use App\Models\Naturezasubitens;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmpenhodetalhadoController extends BaseController
{
    public function ler()
    {
        $nomearquivo = $this->buscaArquivos();

        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);
        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if (substr($nome['nomearquivo'], 0, 7) == 'itensne' or substr($nome['nomearquivo'], 6, 7) == 'itensne') {
                    $listaempenho = $this->lerArquivo($nome['nomearquivo'], $nome['case']);

                    foreach ($listaempenho as $e) {
                        $busca = $this->buscaLista($e['ug'], $e['gestao'], $e['numeroli'], $e['numitem'],
                            $e['subitem']);
                        if (!isset($busca->ug)) {
                            $novo_listaempenho = new Empenhodetalhado;
                            $novo_listaempenho->ug = $e['ug'];
                            $novo_listaempenho->gestao = $e['gestao'];
                            $novo_listaempenho->numeroli = $e['numeroli'];
                            $novo_listaempenho->numitem = $e['numitem'];
                            $novo_listaempenho->subitem = $e['subitem'];
                            $novo_listaempenho->quantidade = $e['quantidade'];
                            $novo_listaempenho->valorunitario = $e['valorunitario'];
                            $novo_listaempenho->valortotal = $e['valortotal'];
                            $novo_listaempenho->descricao = $e['descricao'];
                            $novo_listaempenho->save();
                        }
                    }
                }

            }

            $ok = 'Empenhos Detalhados lidos.';

        } else {
            $ok = 'Não Há arquivos para leitura.';
        }

        return $ok;

    }

    public function buscaLista($ug, $gestao, $numeroli, $numitem, $subitem)
    {

        $listaempenho = Empenhodetalhado::where('ug', $ug)
            ->where('gestao', $gestao)
            ->where('numeroli', $numeroli)
            ->where('numitem', $numitem)
            ->where('subitem', $subitem)
            ->first();

        return $listaempenho;

    }

    public function lerArquivo($nomeaquivo, $case)
    {
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $name = $path . $nomeaquivo;
        $namedestino = $path_processados . $nomeaquivo;

        if($case == 0){
            $extref = ".REF.gz";
            $exttxt = ".TXT.gz";
        }

        if($case == 1){
            $extref = ".ref.gz";
            $exttxt = ".txt.gz";
        }

        $myfileref = gzopen($name . $extref, "r") or die("Unable to open file!");

        $i = 0;
        while (!gzeof($myfileref)) {
            $line = gzgets($myfileref);

            if (strlen($line) == 0) {
                break;
            }

            $ref[$i]['column'] = trim(substr($line, 0, 40));
            $ref[$i]['type'] = trim(substr($line, 40, 1));

            if (strstr(trim(substr($line, 42, 4)), ",") != false) {
                $num = explode(",", trim(substr($line, 42, 4)));
                $ref[$i]['size'] = $num[0] + $num[1];
                $ref[$i]['decimal'] = $num[1];
            } else {
                $ref[$i]['size'] = trim(substr($line, 42, 4)) * 1;
                $ref[$i]['decimal'] = 0;
            }

            $ref[$i]['acu'] = ($i == 0) ? $ref[$i]['size'] : $ref[$i]['size'] + $ref[$i - 1]['acu'];
            $i++;
        }
        $NUMCOLS = $i;
        gzclose($myfileref);


        $myfiletxt = gzopen($name . $exttxt, "r") or die("Unable to open file!");
        $i = 0;
        $j = 0;
        while (!gzeof($myfiletxt)) {
            $line = gzgets($myfiletxt);
            for ($j = 0; $j < $NUMCOLS; $j++) {
                $campo = $ref[$j]['column'];
                $inicio = ($j == 0) ? 0 : $ref[$j - 1]['acu'];
                $valor = trim(substr($line, $inicio, $ref[$j]['size']));
                if ($ref[$j]['type'] == "N") {
                    $valor = $valor * pow(10, -$ref[$j]['decimal']);
                }
                if ($campo == 'GR-UG-GESTAO-AN-NUMERO-LI') {
                    $listaempenho[$i]['ug'] = substr($valor, 0, 6);
                    $listaempenho[$i]['gestao'] = substr($valor, 6, 5);
                    $listaempenho[$i]['numeroli'] = substr($valor, 11, 12);
                }
                if ($campo == 'IT-NU-ITEM') {
                    $listaempenho[$i]['numitem'] = str_pad($valor, 3, "0", STR_PAD_LEFT);
                }
                if ($campo == 'IT-NU-ND-SUBITEM') {
                    $listaempenho[$i]['subitem'] = str_pad($valor, 2, "0", STR_PAD_LEFT);
                }
                if ($campo == 'IT-QT-UNIDADE-ITEM') {
                    $listaempenho[$i]['quantidade'] = number_format($valor, 5, '.', '');
                }
                if ($campo == 'IT-VA-UNIDADE-ITEM') {
                    $listaempenho[$i]['valorunitario'] = number_format($valor, 2, '.', '');
                }
                if ($campo == 'IT-VA-TOTAL-ITEM') {
                    $listaempenho[$i]['valortotal'] = number_format($valor, 2, '.', '');
                }
                if ($campo == 'IT-TX-DESCRICAO-ITEM(1)') {
                    $listaempenho[$i]['descricao'] = strtoupper(utf8_encode($valor));
                }
            }

            $i++;
        }
        gzclose($myfiletxt);

        $this->moverArquivoProcessado($namedestino,$name,$extref);

        $this->moverArquivoProcessado($namedestino,$name,$exttxt);

        return $listaempenho;
    }

    public function buscaEmpenhodetalhadoPorNumeroEmpenho($dado, $nd)
    {
        $ug = substr($dado, 0, 6);
        $gestao = substr($dado, 6, 5);
        $numempenho = strtoupper(substr($dado, 11, 12));
        $retorno = [];

        $empenho = Empenho::where('ug', $ug)
            ->where('gestao', $gestao)
            ->where('numero', $numempenho)
            ->first();

        if (isset($empenho->id)) {

            $empenhodetalhado = $this->buscaEmpenhoDetalhadoPorUgGestaoLi($empenho);

            $i = 0;
            foreach ($empenhodetalhado as $det) {

                $subitem = $this->buscaNaturezaSubitem($nd,$det->subitem);

                $retorno[$i]['numeroli'] = $det->numeroli;
                $retorno[$i]['numitem'] = $det->numitem;
                $retorno[$i]['subitem'] = $subitem['codigo'];
                $retorno[$i]['subitemdescricao'] = $subitem['descricao'];
                $retorno[$i]['quantidade'] = $det->quantidade;
                $retorno[$i]['valorunitario'] = $det->valorunitario;
                $retorno[$i]['valortotal'] = $det->valortotal;
                $i++;
            }

        }

        return $retorno;
    }

    public function buscaNaturezaSubitem(string $nd, $subitem)
    {
        $buscand = Naturezasubitens::where('codigo_nd',$nd)
            ->where('codigo_subitem', $subitem)
            ->first();

        if (!isset($buscand->codigo_nd)) {
            $naturezadespesa['codigo'] = $subitem;
            $naturezadespesa['descricao'] = 'SUBITEM DE DESPESA NÃO CADASTRADO';

            return $naturezadespesa;
        }

        $naturezadespesa['codigo'] = $buscand->codigo_subitem;
        $naturezadespesa['descricao'] = $this->trataString($buscand->descricao_subitem);

        return $naturezadespesa;
    }


    public function buscaEmpenhoDetalhadoPorUgGestaoLi(Empenho $empenho)
    {
        $empenhodetalhado = Empenhodetalhado::where('numeroli', $empenho->num_lista)
            ->get();

        return $empenhodetalhado;
    }

}
