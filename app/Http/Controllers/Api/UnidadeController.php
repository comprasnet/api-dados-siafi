<?php

namespace App\Http\Controllers\Api;

use App\Models\Unidade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UnidadeController extends BaseController
{
    public function ler()
    {
        $nomearquivo = $this->buscaArquivos();

        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);
        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if (substr($nome['nomearquivo'], 0, 2) == 'ug' or substr($nome['nomearquivo'], 6, 2) == 'ug') {
                    $credor = $this->lerArquivo($nome['nomearquivo'], $nome['case']);

                    foreach ($credor as $e) {
                        $busca = $this->buscaUnidade($e['codigo']);
                        if (!isset($busca->codigo)) {
                            $nova_unidade = new Unidade;
                            $nova_unidade->codigo = $e['codigo'];
                            $nova_unidade->cnpj = $e['cnpj'];
                            $nova_unidade->funcao = $e['funcao'];
                            $nova_unidade->nome = $e['nome'];
                            $nova_unidade->nomeresumido = $e['nomeresumido'];
                            $nova_unidade->uf = $e['uf'];
                            $nova_unidade->orgao = $e['orgao'];
                            $nova_unidade->save();
                        } else {
                            $busca->cnpj = $e['cnpj'];
                            $busca->funcao = $e['funcao'];
                            $busca->nomeresumido = $e['nomeresumido'];
                            $busca->uf = $e['uf'];
                            $busca->orgao = $e['orgao'];
                            $busca->save();
                        }
                    }
                }

            }

            $ok = 'Unidades lidas.';
        } else {
            $ok = 'Não Há arquivos para leitura.';
        }

        return $ok;

    }

    public function lerArquivo($nomeaquivo, $case)
    {
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $name = $path . $nomeaquivo;
        $namedestino = $path_processados . $nomeaquivo;

        if ($case == 0) {
            $extref = ".REF.gz";
            $exttxt = ".TXT.gz";
        }

        if ($case == 1) {
            $extref = ".ref.gz";
            $exttxt = ".txt.gz";
        }

        $myfileref = gzopen($name . $extref, "r") or die("Unable to open file!");

        $i = 0;
        while (!gzeof($myfileref)) {
            $line = gzgets($myfileref);

            if (strlen($line) == 0) break;

            $ref[$i]['column'] = trim(substr($line, 0, 40));
            $ref[$i]['type'] = trim(substr($line, 40, 1));

            if (strstr(trim(substr($line, 42, 4)), ",") != FALSE) {
                $num = explode(",", trim(substr($line, 42, 4)));
                $ref[$i]['size'] = $num[0] + $num[1];
                $ref[$i]['decimal'] = $num[1];
            } else {
                $ref[$i]['size'] = trim(substr($line, 42, 4)) * 1;
                $ref[$i]['decimal'] = 0;
            }

            $ref[$i]['acu'] = ($i == 0) ? $ref[$i]['size'] : $ref[$i]['size'] + $ref[$i - 1]['acu'];
            $i++;
        }
        $NUMCOLS = $i;
        gzclose($myfileref);


        $myfiletxt = gzopen($name . $exttxt, "r") or die("Unable to open file!");
        $i = 0;
        $j = 0;
        while (!gzeof($myfiletxt)) {
            $line = gzgets($myfiletxt);
            for ($j = 0; $j < $NUMCOLS; $j++) {
                $campo = $ref[$j]['column'];
                $inicio = ($j == 0) ? 0 : $ref[$j - 1]['acu'];
                $valor = trim(substr($line, $inicio, $ref[$j]['size']));
                if ($ref[$j]['type'] == "N") {
                    $valor = $valor * pow(10, -$ref[$j]['decimal']);
                }
                if ($campo == 'IT-CO-UNIDADE-GESTORA') {
                    $credor[$i]['codigo'] = str_pad(substr($valor, 0, 6), 6, "0", STR_PAD_LEFT);
                }
                if ($campo == 'IT-NO-UNIDADE-GESTORA') {
                    $credor[$i]['nome'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'IT-NO-MNEMONICO-UNIDADE-GESTORA') {
                    $credor[$i]['nomeresumido'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'IT-NU-CGC') {
                    $credor[$i]['cnpj'] = str_pad($valor, 14, "0", STR_PAD_LEFT);
                }
                if ($campo == 'IT-CO-UF') {
                    $credor[$i]['uf'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'GR-ORGAO') {
                    $credor[$i]['orgao'] = str_pad(substr($valor, 0, 5), 5, "0", STR_PAD_LEFT);
                }
                if ($campo == 'IT-IN-FUNCAO-UG') {
                    $credor[$i]['funcao'] = $valor;
                }
            }

            $i++;
        }

        gzclose($myfiletxt);

        $this->moverArquivoProcessado($namedestino, $name, $extref);

        $this->moverArquivoProcessado($namedestino, $name, $exttxt);

        return $credor;
    }

    public function buscaUnidade($codigo)
    {

        $credor = Unidade::where('codigo', $codigo)
            ->first();

        return $credor;

    }

    /**
     * @OA\Get(
     *     path="/api/unidade/empenho/{ano}/{data}",
     *     tags={"Unidades"},
     *     summary="Retorna unidades com empenhos.",
     *     description="Retorna as unidades cuja função é 1 (Executora) que possuem empenhos:
      <ul>
      <li>emitidos no ano informado</li>
      <li>alterados (updated_at) após a data recebida.</li>
      <li>cujo número inicia com o ano recebido seguido de 'NE'. Ex.:  2023NE*</li>
      </ul>
      Se não receber data, define 01/01/{ano recebido}.",
     * @OA\Parameter(
     *         name="ano",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             format="integer",
     *             description="Ano do empenho"
     *         )
     *     ),
     * @OA\Parameter(
     *         name="data",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             format="date",
     *             pattern="^\d{4}-\d{2}-\d{2}$",
     *             description="Data no formato yyyy-mm-dd"
     *         ),
     *         example="2024-01-01"
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Unidades com empenhos",
     *         @OA\JsonContent(
     *             type="array",
     *             example={
     *                 {"codigo": "123456"},
     *                 {"codigo": "234567"},
     *                 {"codigo": "345678"}
     *             },
     *             @OA\Items(
     *                 @OA\Property(
     *                     property="codigo",
     *                     type="string",
     *                     description="Descrição da chave 'codigo'."
     *                 )
     *             )
     *         ),
     *     ),
     * @OA\Response(
     *         response=401,
     *         description="Error: Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *     ),
     * )
     *
     */
    public function getUnidadesComEmpenhos($ano, $data = null)
    {
        $ano = $ano ?? date('Y');
        $data = $data ?? $ano . '-01-01';
        $unidades = Unidade::select('unidades.codigo')
            ->distinct()
            ->join('empenhos', 'empenhos.ug', '=', 'unidades.codigo')
            ->where('empenhos.updated_at', '>=', $data)
            ->where('ano_emissao', $ano)
            ->where('empenhos.numero', 'LIKE', $ano . 'NE%')
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('unidades.id_sistema_origem', 'SIAFI')
                        ->where('unidades.funcao', '=', '1');
                })
                    ->orWhere('unidades.id_sistema_origem', '<>', 'SIAFI');
            })
            ->get();

        $unidadesPares = [];
        $unidadesImpares = [];

        foreach ($unidades as $unidade) {
            if ($unidade->codigo % 2 == 0) {
                $unidadesPares[] = $unidade;
            } else {
                $unidadesImpares[] = $unidade;
            }
        }

        // Verifica se o dia é par ou ímpar
        $hoje = date('j'); // Dia do mês atual
        $metadeSelecionada = ($hoje % 2 == 0) ? $unidadesPares : $unidadesImpares;

        return response()->json($metadeSelecionada, 200);
    }

    /**
     * @OA\Get(
     *     path="/api/unidade/rp/{ano}/{data}",
     *     tags={"Unidades"},
     *     summary="Retorna unidades com restos a pagar.",
     *     description="Retorna as unidades que possuem empenhos:
    <ul>
    <li>emitidos no ano informado</li>
    <li>alterados (updated_at) após a data recebida.</li>
    <li>cujo número de empenho inicia com um ano diferente do ano recebido, seguido de 'NE'. Ex.:  2023NE*</li>
    </ul>
    Se não receber data, define 01/01/{ano recebido}.",
     *     @OA\Parameter(
     *         name="ano",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             format="integer",
     *             description="Ano do empenho"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="data",
     *         in="path",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             format="date",
     *             pattern="^\d{4}-\d{2}-\d{2}$",
     *             description="Data no formato yyyy-mm-dd"
     *         ),
     *         example="2024-01-01"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Unidades com empenhos",
     *         @OA\JsonContent(
     *             type="array",
     *             example={
     *                 {"codigo": "123456"},
     *                 {"codigo": "234567"},
     *                 {"codigo": "345678"}
     *             },
     *             @OA\Items(
     *                 @OA\Property(
     *                     property="codigo",
     *                     type="string",
     *                     description="Descrição da chave 'codigo'."
     *                 )
     *             )
     *         ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error: Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *     ),
     * )
     *
     */
    public function getUnidadesComRP($ano, $data = null)
    {
        $ano = $ano ?? date('Y');
        $data = $data ?? $ano . '-01-01';

        $unidades = Unidade::select('unidades.codigo')
            ->distinct()
            ->join('empenhos', 'empenhos.ug', '=', 'unidades.codigo')
            ->where(DB::raw('left(empenhos."numero", 4)'), '<>', $ano)
            ->where('ano_emissao', $ano)
            ->where('empenhos.updated_at', '>=', $data)
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('unidades.id_sistema_origem', 'SIAFI')
                        ->where('unidades.funcao', '=', '1');
                })
                    ->orWhere('unidades.id_sistema_origem', '<>', 'SIAFI');
            })
            ->get();

        return response()->json($unidades, 200);
    }


    // desconsiderar gestão quando id_sistema_origem nãor for SIAFI
    public function buscarDocumentoOrigemPorUg(string $ug, string $gestao, string $documento)
    {
        
        return Unidade::select('ordembancaria.*')
        ->distinct()
        ->join('ordembancaria', 'ordembancaria.ug', '=', 'unidades.codigo')
        ->where('ordembancaria.ug', $ug)
        ->where(function ($query) use ($gestao) {
            $query->where(function ($subQuery) use ($gestao) {
                $subQuery->where('unidades.id_sistema_origem', 'SIAFI')
                        ->where('ordembancaria.gestao', $gestao);
            })
            ->orWhere('unidades.id_sistema_origem', '<>', 'SIAFI');
        })
        ->where('documentoorigem', $documento)
        ->get();
      
    }
}
