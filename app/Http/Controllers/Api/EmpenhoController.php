<?php

namespace App\Http\Controllers\Api;

use App\Models\Credor;
use App\Models\Empenhodetalhado;
use App\Models\Naturezasubitens;
use App\Models\Obxne;
use App\Models\Ordembancaria;
use App\Models\Planointerno;
use App\Models\Unidade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Empenho;
use stdClass;


class EmpenhoController extends BaseController
{

    public function ler()
    {
        $nomearquivo = $this->buscaArquivos();

        $pkCount = (is_array($nomearquivo) ? count($nomearquivo) : 0);

        if ($pkCount != 0) {
            foreach ($nomearquivo as $nome) {
                if (substr($nome['nomearquivo'], 0, 2) == 'ne'
                    or substr($nome['nomearquivo'], 6, 2) == 'ne') {
                    $empenho = $this->lerArquivo($nome['nomearquivo'], $nome['case']);


                }
                foreach ($empenho as $e) {
                    $busca = $this->buscaEmpenho($e['ug'], $e['gestao'], $e['numero']);
                    if (!isset($busca->numero)) {
                        $novo_empenho = new Empenho;
                        $novo_empenho->ug = $e['ug'];
                        $novo_empenho->gestao = $e['gestao'];
                        $novo_empenho->numero = $e['numero'];
                        $novo_empenho->numero_ref = $e['numero_ref'];
                        $novo_empenho->emissao = $e['emissao'];
                        $novo_empenho->tipofavorecido = $e['tipofavorecido'];
                        $novo_empenho->favorecido = $e['favorecido'];
                        $novo_empenho->observacao = $e['observacao'];
                        $novo_empenho->fonte = $e['fonte'];
                        $novo_empenho->naturezadespesa = $e['naturezadespesa'];
                        $novo_empenho->planointerno = $e['planointerno'];
                        $novo_empenho->num_lista = $e['num_lista'];
                        $novo_empenho->save();
                    }
                }

            }

            $ok = 'Empenhos lidos.';

        } else {
            $ok = 'Não Há arquivos para leitura.';
        }

        return $ok;

    }

    public function lerArquivo($nomeaquivo, $case)
    {
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $name = $path . $nomeaquivo;
        $namedestino = $path_processados . $nomeaquivo;

        if ($case == 0) {
            $extref = ".REF.gz";
            $exttxt = ".TXT.gz";
        }

        if ($case == 1) {
            $extref = ".ref.gz";
            $exttxt = ".txt.gz";
        }

        $myfileref = gzopen($name . $extref, "r") or die("Unable to open file!");

        $i = 0;
        while (!gzeof($myfileref)) {
            $line = gzgets($myfileref);

            if (strlen($line) == 0) {
                break;
            }

            $ref[$i]['column'] = trim(substr($line, 0, 40));
            $ref[$i]['type'] = trim(substr($line, 40, 1));

            if (strstr(trim(substr($line, 42, 4)), ",") != false) {
                $num = explode(",", trim(substr($line, 42, 4)));
                $ref[$i]['size'] = $num[0] + $num[1];
                $ref[$i]['decimal'] = $num[1];
            } else {
                $ref[$i]['size'] = trim(substr($line, 42, 4)) * 1;
                $ref[$i]['decimal'] = 0;
            }

            $ref[$i]['acu'] = ($i == 0) ? $ref[$i]['size'] : $ref[$i]['size'] + $ref[$i - 1]['acu'];
            $i++;
        }
        $NUMCOLS = $i;
        gzclose($myfileref);

        $myfiletxt = gzopen($name . $exttxt, "r") or die("Unable to open file!");
        $i = 0;
        $j = 0;
        while (!gzeof($myfiletxt)) {
            $line = gzgets($myfiletxt);
            for ($j = 0; $j < $NUMCOLS; $j++) {
                $campo = $ref[$j]['column'];
                $inicio = ($j == 0) ? 0 : $ref[$j - 1]['acu'];
                $valor = trim(substr($line, $inicio, $ref[$j]['size']));
                if ($ref[$j]['type'] == "N") {
                    $valor = $valor * pow(10, -$ref[$j]['decimal']);
                }
                if ($campo == 'GR-UG-GESTAO-AN-NUMERO-NEUQ(1)') {
                    $empenho[$i]['ug'] = substr($valor, 0, 6);
                    $empenho[$i]['gestao'] = substr($valor, 6, 5);
                    $empenho[$i]['numero'] = substr($valor, 11, 12);
                }
                if ($campo == 'GR-AN-NU-DOCUMENTO-REFERENCIA') {
                    $empenho[$i]['numero_ref'] = $valor;
                }
                if ($campo == 'IT-DA-EMISSAO') {
                    $empenho[$i]['emissao'] =
                        substr($valor, 0, 4) . '-' .
                        substr($valor, 4, 2) . '-' .
                        substr($valor,
                            6, 2);
                }
                if ($campo == 'IT-IN-FAVORECIDO') {
                    $empenho[$i]['tipofavorecido'] = $valor;
                }
                if ($campo == 'IT-CO-FAVORECIDO') {
                    if ($empenho[$i]['tipofavorecido'] == 4) {
                        $empenho[$i]['favorecido'] = substr($valor, 0, 6);
                    } else {
                        $empenho[$i]['favorecido'] = $valor;
                    }
                }
                if ($campo == 'IT-TX-OBSERVACAO') {
                    $empenho[$i]['observacao'] = strtoupper(utf8_encode($valor));
                }
                if ($campo == 'GR-FONTE-RECURSO') {
                    $empenho[$i]['fonte'] = $valor;
                }
                if ($campo == 'GR-NATUREZA-DESPESA') {
                    $empenho[$i]['naturezadespesa'] = $valor;
                }
                if ($campo == 'IT-CO-PLANO-INTERNO') {
                    $empenho[$i]['planointerno'] = $valor;
                }
                if ($campo == 'IT-NU-LISTA(1)') {
                    $empenho[$i]['num_lista'] = $valor;
                }
            }

            if ($empenho[$i]['fonte'] == '' and $empenho[$i]['naturezadespesa'] == '0') {
                unset($empenho[$i]);
            }
            $i++;
        }
        gzclose($myfiletxt);

        $this->moverArquivoProcessado($namedestino, $name, $extref);

        $this->moverArquivoProcessado($namedestino, $name, $exttxt);


        return $empenho;
    }

    public function buscaEmpenho($ug, $gestao, $numero)
    {

        $empenho = Empenho::where('ug', $ug)
            ->where('gestao', $gestao)
            ->where('numero', $numero)
            ->first();

        return $empenho;

    }

    /**
     * @OA\Get(
     *      path="/api/empenho/{dado}",
     *      operationId="buscaEmpenhoPorNumero",
     *      tags={"Empenhos"},
     *      summary="Retorna um empenho filtrado pelos dados enviados.",
     *      description="Retorna um empenho filtrado com base nos dados enviados, os quais devem ser fornecidos
    concatenados em uma string. (exemplo: 123456543212024NE246135), onde:<br>
     *     Os 6 primeiros dígitos são o código da Unidade Gestora (Ex.: 123456).<br>
     *     Os 5 próximos dígitos são o código da gestão (Ex.: 54321).<br>
     *     Os 12 próximos dígitos são o número do empenho (Ex.: 2024NE246135).<br>",
     * @OA\Parameter(
     *          name="dado",
     *          in="path",
     *          required=true,
     *          description="String de dados",
     *          example="123456543212024NE246135",
     *          @OA\Schema(type="string")
     *      ),
     * @OA\Response(
     *          response=200,
     *          description="Operação bem-sucedida",
     *          @OA\JsonContent(ref="#/components/schemas/Empenho")
     *      ),
     * @OA\Response(
     *          response=401,
     *          description="Error: Unauthorized",
     *          @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *      ),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     * )
     */
    public function buscaEmpenhoPorNumero($dado)
    {
        $ug = substr($dado, 0, 6);
        $gestao = substr($dado, 6, 5);
        $numempenho = strtoupper(substr($dado, 11, 12));
        $ano = substr($numempenho, 0, 4);

        if (!is_numeric($ano)) {
            return response()->json([], 400);
        }

        if ($ano < '2021') {
            $empenhos = $this->buscaEmpenhoAnteriorA2021($ug, $gestao, $numempenho);
        } else {
            $empenhos = $this->buscaEmpenhoPosteriorA2021($ug, $gestao, $numempenho, $ano);
        }

        return response()->json($empenhos);

    }

    /**
     * @OA\Get(
     *      path="/api/empenho/dadosbasicos/{dado}",
     *      operationId="buscaEmpenhoPorNumeroDadosBasicos",
     *      tags={"Empenhos"},
     *      summary="Retorna um empenho filtrado pelos dados enviados.",
     *      description="Retorna um empenho filtrado com base nos dados enviados, os quais devem ser enviados
    concatenados em uma string. (exemplo: 123456543212024NE246135), onde:<br>
     *     Os 6 primeiros dígitos são o código da Unidade Gestora (Ex.: 123456).<br>
     *     Os 5 próximos dígitos são o código da gestão (Ex.: 54321).<br>
     *     Os 12 próximos dígitos são o número do empenho (Ex.: 2024NE246135).<br>",
     * @OA\Parameter(
     *          name="dado",
     *          in="path",
     *          required=true,
     *          description="String de dados",
     *          example="123456543212024NE246135",
     *          @OA\Schema(type="string")
     *      ),
     * @OA\Response(
     *          response=200,
     *          description="Operação bem-sucedida",
     *          @OA\JsonContent(ref="#/components/schemas/EmpenhoUnico")
     *      ),
     * @OA\Response(
     *          response=401,
     *          description="Error: Unauthorized",
     *          @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *      ),
     *      security={
     *          {"bearerAuth": {}}
     *      },
     * )
     */
    public function buscaEmpenhoPorNumeroDadosBasicos($dado)
    {
        $ug = substr($dado, 0, 6);
        $gestao = substr($dado, 6, 5);
        $numempenho = strtoupper(substr($dado, 11, 12));
        $ano = substr($numempenho, 0, 4);

        $empenho = Empenho::select([
            'empenhos.num_lista',
            'empenhos.ug',
            'empenhos.gestao',
            'empenhos.numero',
            'empenhos.emissao',
            'ano_emissao'
            , 'empenhos.tipofavorecido as tipocredor'
            , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
            , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
            , 'empenhos.observacao'
            , 'empenhos.fonte'
            , 'empenhos.modalidade_licitacao_siafi'
            , 'empenhos.ptres'
            , 'empenhos.naturezadespesa'
            , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
            , 'empenhos.planointerno as picodigo'
            , 'planointerno.descricao as pidescricao'
            , 'empenhos.sistemaorigem'
            , 'empenhos.informacaocomplementar'
        ])
            ->leftJoin('naturezasubitens', 'naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
            ->leftJoin('planointerno', 'planointerno.codigo', '=', 'empenhos.planointerno')
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor',
                DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"),
                '=',
                'empenhos.favorecido')
            ->where('empenhos.ug', $ug)
            ->where(function ($query) use ($gestao) {
                $query->where(function ($query) use ($gestao) {
                    $query->where('empenhos.id_sistema_origem', 'SIAFI')
                        ->where('empenhos.gestao', $gestao);
                })
                    ->orWhere('empenhos.id_sistema_origem', '<>', 'SIAFI');
            })
            ->where('empenhos.numero', $numempenho)
            ->where('empenhos.ano_emissao', $ano)
            ->first();

        return response()->json($empenho);

    }

    private function buscaEmpenhoAnteriorA2021($ug, $gestao, $numempenho)
    {
        return Empenho::
        with(
            array('itens' => function ($query) {
                $query->select(
                    'empenhodetalhado.numeroli',
                    'empenhodetalhado.numitem',
                    'empenhodetalhado.subitem',
                    DB::raw('naturezasubitens.descricao_subitem as subitemdescricao'),
                    'empenhodetalhado.quantidade',
                    'empenhodetalhado.valorunitario',
                    'empenhodetalhado.valortotal'
                )
                    ->join('empenhos', 'empenhos.num_lista', '=', 'empenhodetalhado.numeroli')
                    ->join('naturezasubitens', function ($join) {
                        $join->on('naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
                            ->on('naturezasubitens.codigo_subitem', '=', 'empenhodetalhado.subitem')
                            ->on('naturezasubitens.sistema_origem', '=', 'empenhodetalhado.id_sistema_origem');
                    })
                    ->distinct();
            })
        )
            ->select(['empenhos.num_lista', 'empenhos.ug', 'empenhos.gestao', 'empenhos.numero', 'empenhos.emissao', 'ano_emissao'
                , 'empenhos.tipofavorecido as tipocredor'
                , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
                , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
                , 'empenhos.observacao'
                , 'empenhos.fonte'
                , 'empenhos.modalidade_licitacao_siafi'
                , 'empenhos.ptres'
                , 'empenhos.naturezadespesa'
                , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
                , 'empenhos.planointerno as picodigo'
                , 'planointerno.descricao as pidescricao'
                , 'empenhos.sistemaorigem'
                , 'empenhos.informacaocomplementar'

            ])
            ->distinct()
            ->leftJoin('naturezasubitens', 'naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
            ->leftJoin('planointerno', 'planointerno.codigo', '=', 'empenhos.planointerno')
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor', DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"), '=', 'empenhos.favorecido')
            ->where('ug', $ug)
            #->where('gestao', $gestao)
            ->where(function ($query) use ($gestao) {
                $query->where(function ($query) use ($gestao) {
                    $query->where('empenhos.id_sistema_origem', 'SIAFI')
                        ->where('empenhos.gestao', $gestao);
                })
                    ->orWhere('empenhos.id_sistema_origem', '<>', 'SIAFI');
            })
            ->where('numero', $numempenho)
            ->first();
    }

    private function buscaEmpenhoPosteriorA2021($ug, $gestao, $numempenho, $ano)
    {
        return Empenho::
        with(
            array('itens' => function ($query) use ($ano) {

                $query->select(
                    'empenhodetalhado.numeroli',
                    'empenhodetalhado.numitem',
                    'empenhodetalhado.subitem',
                    DB::raw('naturezasubitens.descricao_subitem as subitemdescricao'),
                    DB::raw("case
                                        when (sum(empenhodetalhado.valortotal) = 0 OR sum(empenhodetalhado.quantidade) = 0)
                                            then
                                            round(sum(empenhodetalhado.valorunitario) / count(empenhodetalhado.numeroli),2)
                                        else
                                            round(sum(empenhodetalhado.valortotal) / sum(empenhodetalhado.quantidade),2)
                                        end as valorunitario
                                    "
                    ),
                    DB::raw("sum(empenhodetalhado.quantidade) as quantidade"),
                    DB::raw("sum(empenhodetalhado.valortotal) as valortotal")
                )
                    ->join('empenhos', function ($join) {
                        $join->on('empenhos.num_lista', '=', 'empenhodetalhado.numeroli')
                            ->on('empenhos.ano_emissao', '=', 'empenhodetalhado.ano_emissao');
                    })
                    ->join('naturezasubitens', function ($join) {
                        $join->on('naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
                            ->on('naturezasubitens.codigo_subitem', '=', 'empenhodetalhado.subitem')
                            ->on('naturezasubitens.sistema_origem', '=', 'empenhodetalhado.id_sistema_origem');
                    })
                    ->groupBy(
                        'empenhodetalhado.numeroli',
                        'empenhodetalhado.numitem',
                        'empenhodetalhado.subitem',
                        'naturezasubitens.descricao_subitem'
                    );
                    /* ->where('empenhodetalhado.ano_emissao', DB::raw($ano)); */
            })
        )
            ->select(['empenhos.num_lista', 'empenhos.ug', 'empenhos.gestao', 'empenhos.numero', 'empenhos.emissao', 'ano_emissao'
                , 'empenhos.tipofavorecido as tipocredor'
                , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
                , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
                , 'empenhos.observacao'
                , 'empenhos.fonte'
                , 'empenhos.modalidade_licitacao_siafi'
                , 'empenhos.ptres'
                , 'empenhos.naturezadespesa'
                , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
                , 'empenhos.planointerno as picodigo'
                , 'planointerno.descricao as pidescricao'
                , 'empenhos.sistemaorigem'
                , 'empenhos.informacaocomplementar'
            ])
            ->distinct()
            ->leftJoin('naturezasubitens', function ($j) {
                $j->on('naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
                    ->on('naturezasubitens.sistema_origem', '=', 'empenhos.id_sistema_origem');
            })
            ->leftJoin('planointerno', function ($j) {
                $j->on('planointerno.codigo', '=', 'empenhos.planointerno')
                    ->on('planointerno.sistema_origem', '=', 'empenhos.id_sistema_origem');
            })
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor', DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"), '=', 'empenhos.favorecido')
            ->where('empenhos.ug', $ug)
            #->where('empenhos.gestao', $gestao)
            ->where('empenhos.numero', $numempenho)
            ->where(function ($query) use ($gestao) {
                $query->where(function ($query) use ($gestao) {
                    $query->where('empenhos.id_sistema_origem', 'SIAFI')
                        ->where('empenhos.gestao', $gestao);
                })
                    ->orWhere('empenhos.id_sistema_origem', '<>', 'SIAFI');
            })
            /* ->where('empenhos.ano_emissao', $ano) */
            ->first();
    }

    public function buscaCredor(string $dado, string $tipo)
    {
        if ($tipo == '4') {
            return $this->buscaCredorUnidade($dado);
        }

        return $this->buscaCredorFavorecido($dado, $tipo);
    }

    public function buscaCredorUnidade(string $ug)
    {
        $unidade = Unidade::where('codigo', $ug)
            ->first();

        if (!isset($unidade->codigo)) {
            $credor['codigo'] = $ug;
            $credor['nome'] = 'CREDOR SEM CADASTRO';

            return $credor;
        }

        $credor['codigo'] = $unidade->codigo;
        $credor['nome'] = $this->trataString($unidade->nome);

        return $credor;
    }

    public function buscaCredorFavorecido(string $dado, string $tipo)
    {
        $dado = $this->formataCnpjCpfTipo($dado, $tipo); //precisa comentar essa linha

        $favorecido = Credor::where('cpf_cnpj_idgener', $dado)
            ->first();

        if (!isset($favorecido->cpf_cnpj_idgener)) {
//            $credor['codigo'] = $this->formataCnpjCpfTipo($dado, $tipo);
            $credor['codigo'] = $dado;
            $credor['nome'] = 'CREDOR SEM CADASTRO';

            return $credor;
        }

//        $credor['codigo'] = $this->formataCnpjCpfTipo($favorecido->cpf_cnpj_idgener, $tipo);
        $credor['codigo'] = $favorecido->cpf_cnpj_idgener;
        $credor['nome'] = $this->trataString($favorecido->nome);

        return $credor;
    }

    public function buscaPlanointerno(string $pi)
    {
        $buscapi = Planointerno::where('codigo', $pi)
            ->first();

        if (!isset($buscapi->codigo)) {
            $planointerno['codigo'] = $pi;
            $planointerno['descricao'] = 'PLANO INTERNO NÃO CADASTRADO';
            $planointerno['sistema_origem'] = null;

            return $planointerno;
        }

        $planointerno['codigo'] = $buscapi->codigo;
        $planointerno['descricao'] = $this->trataString($buscapi->descricao);
        $planointerno['sistema_origem'] = $buscapi->sistema_origem;

        return $planointerno;
    }

    /**
     * @OA\Get(
     *      path="/api/empenho/ano/{ano}/ug/{ug}/dia/{data}",
     *      operationId="buscaEmpenhoPorDiaUg",
     *      tags={"Empenhos"},
     *      summary="Retorna empenhos por unidade gestora.",
     *      description="Retorna os empenhos e itens de uma unidade gestora,
    cuja atualização (updated_at) vai desde a data fornecida até cinco dias anteriores.<br>
    Também filtra os empenhos emitidos no ano fornecido
    e que comecem com este ano, seguido de 'NE'.
    Ex.: 2023NE*",
     *      @OA\Parameter(
     *          name="ano",
     *          in="path",
     *          required=true,
     *          description="Ano do empenho",
     *          example="2024",
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *          name="ug",
     *          in="path",
     *          required=true,
     *          description="Unidade gestora",
     *          example="123456",
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *          name="data",
     *          in="path",
     *          required=false,
     *          description="Data do empenho",
     *          @OA\Schema(
     *              type="string",
     *              format="date",
     *              pattern="^\d{4}-\d{2}-\d{2}$",
     *              description="Data no formato yyyy-mm-dd"
     *          ),
     *          example="2024-01-01"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Operação bem-sucedida",
     *          @OA\JsonContent(ref="#/components/schemas/ArrayDeEmpenhosComItens")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Error: Unauthorized",
     *          @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *      ),
     *      security={
     *          {"bearerAuth": {}}
     *      },
     * )
     */
    public function buscaEmpenhoPorDiaUg($ano, $ug, $data = null)
    {
        if (!$data) {
            $data = date('Y-m-d');
        }
        $dataCarbon = Carbon::parse($data);//->subDay(5);

        return Empenho::
        with(
            array('itens' => function ($query) use ($ano) {

                $query->select(
                    'empenhodetalhado.numeroli',
                    'empenhodetalhado.numitem',
                    'empenhodetalhado.subitem',
                    DB::raw('naturezasubitens.descricao_subitem as subitemdescricao'),
                    DB::raw("
                                case
                                    when
                                        (sum(empenhodetalhado.valortotal) = 0 OR sum(empenhodetalhado.quantidade) = 0)
                                    then
                                        round(sum(empenhodetalhado.valorunitario) / count(empenhodetalhado.numeroli),2)
                                    else
                                        round(sum(empenhodetalhado.valortotal) / sum(empenhodetalhado.quantidade),2)
                                end as valorunitario

                             "),
                    DB::raw("sum(empenhodetalhado.quantidade) as quantidade"),
                    DB::raw("sum(empenhodetalhado.valortotal) as valortotal")
                )
                    ->join('empenhos', function ($join) {
                        $join->on('empenhos.num_lista', '=', 'empenhodetalhado.numeroli')
                            ->on('empenhos.ano_emissao', '=', 'empenhodetalhado.ano_emissao');
                    })
                    ->join('naturezasubitens', function ($join) {
                        $join->on('naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
                            ->on('naturezasubitens.codigo_subitem', '=', 'empenhodetalhado.subitem')
                            ->on('naturezasubitens.sistema_origem', '=', 'empenhos.id_sistema_origem');
                    })
                    ->groupBy(
                        'empenhodetalhado.numeroli',
                        'empenhodetalhado.numitem',
                        'empenhodetalhado.subitem',
                        'naturezasubitens.descricao_subitem'
                    )
                    ->where('empenhodetalhado.ano_emissao', DB::raw($ano));
            })
        )
            ->select(['empenhos.num_lista', 'empenhos.ug', 'empenhos.gestao', 'empenhos.numero', 'empenhos.emissao', 'ano_emissao'
                , 'empenhos.tipofavorecido as tipocredor'
                , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
                , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
                , 'empenhos.observacao'
                , 'empenhos.fonte'
                , 'empenhos.modalidade_licitacao_siafi'
                , 'empenhos.ptres'
                , 'empenhos.naturezadespesa'
                , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
                , 'naturezasubitens.sistema_origem as nd_sistema_origem'
                , 'empenhos.planointerno as picodigo'
                , 'planointerno.descricao as pidescricao'
                , 'planointerno.sistema_origem as pi_sistema_origem'
                , 'empenhos.sistemaorigem'
                , 'empenhos.id_sistema_origem'
                , 'empenhos.informacaocomplementar'
                , 'empenhos.ano_emissao'
            ])
            ->distinct()
            ->leftJoin('naturezasubitens', function ($j) {
                $j->on('naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
                    ->on('naturezasubitens.sistema_origem', '=', 'empenhos.id_sistema_origem');
            })
            ->leftJoin('planointerno', function ($j) {
                $j->on('planointerno.codigo', '=', 'empenhos.planointerno')
                    ->on('planointerno.sistema_origem', '=', 'empenhos.id_sistema_origem');
            })
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor',
                DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"),
                '=',
                'empenhos.favorecido')
            ->where('empenhos.ug', $ug)
            ->where('empenhos.updated_at', '>=', $dataCarbon)
            ->where('empenhos.ano_emissao', $ano)
            ->where('empenhos.numero', 'LIKE', $ano . '%')
            ->get();
    }

    public function buscaEmpenhoPorDiaUg_old($ano, $ug, $data = null)
    {

        $dataMenosCinco = Carbon::parse($data)->subDay(5);

        $empenhos = Empenho::
        with(
            array('itens' => function ($query) {

                $sub = Naturezasubitens::select('descricao_subitem')
                    ->where('codigo_nd', DB::raw("empenhos.naturezadespesa"))
                    ->where('codigo_subitem', DB::raw("empenhodetalhado.subitem"));

                $query->select(
                    'empenhodetalhado.numeroli',
                    'empenhodetalhado.numitem',
                    'empenhodetalhado.subitem',
                    'empenhodetalhado.quantidade',
                    'empenhodetalhado.valorunitario',
                    'empenhodetalhado.valortotal',

                    (DB::raw(
                        "(" . $sub->toSql() . ") as subitemdescricao"
                    ))
                )
                    ->join('empenhos', 'empenhos.num_lista', '=', 'empenhodetalhado.numeroli');
            })
        )
            ->select(['empenhos.num_lista', 'empenhos.ug', 'empenhos.gestao', 'empenhos.numero', 'empenhos.emissao'
                , 'empenhos.tipofavorecido as tipocredor'
                , DB::raw('case
                               when empenhos.tipofavorecido = \'1\'
                                   then
                                       substr(empenhos.favorecido, 1, 2) || \'.\' || SUBSTR(empenhos.favorecido, 3, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 6, 3) || \'/\' || SUBSTR(empenhos.favorecido, 9, 4) || \'-\' ||
                                       substr(empenhos.favorecido, 13)
                               when empenhos.tipofavorecido = \'2\'
                                   then
                                       substr(empenhos.favorecido, 1, 3) || \'.\' || substr(empenhos.favorecido, 4, 3) || \'.\' ||
                                       substr(empenhos.favorecido, 7, 3) || \'-\' || substr(empenhos.favorecido, 10)
                               else
                                   empenhos.favorecido
                               end          as cpfcnpjugidgener')
                , DB::raw('case
                               when empenhos.tipofavorecido = \'4\'
                                   then
                                   unidades.nome
                               else
                                   credor.nome
                               end          as nome
                               ')
                , 'empenhos.observacao'
                , 'empenhos.fonte'
                , 'empenhos.naturezadespesa'
                , 'naturezasubitens.descricao_nd as naturezadespesadescricao'
                , 'empenhos.planointerno as picodigo'
                , 'planointerno.descricao as pidescricao'

            ])
            ->distinct()
            ->leftJoin('naturezasubitens', function ($j) {
                $j->on('naturezasubitens.codigo_nd', '=', 'empenhos.naturezadespesa')
                    ->on('naturezasubitens.sistema_origem', '=', 'empenhos.id_sistema_origem');
            })
            ->leftJoin('planointerno', function ($j) {
                $j->on('planointerno.codigo', '=', 'empenhos.planointerno')
                    ->on('planointerno.sistema_origem', '=', 'empenhos.id_sistema_origem');
            })
            ->leftJoin('unidades', 'unidades.codigo', '=', 'empenhos.favorecido')
            ->leftJoin('credor',
                DB::raw("replace(replace(replace(credor.cpf_cnpj_idgener, '-', ''), '.', ''), '/', '')"),
                '=',
                'empenhos.favorecido')
            ->where('ug', $ug)
            ->where('empenhos.updated_at', '>=', $dataMenosCinco)
            ->where('numero', 'LIKE', $ano . '%')
            ->get();
        return response()->json($empenhos);
    }

    public function buscaNatureza(string $nd)
    {
        $buscand = Naturezasubitens::where('codigo_nd', $nd)
            ->first();

        if (!isset($buscand->codigo_nd)) {
            $naturezadespesa['codigo'] = $nd;
            $naturezadespesa['descricao'] = 'NATUREZA DE DESPESA NÃO CADASTRADA';
            $naturezadespesa['sistema_origem'] = null;

            return $naturezadespesa;
        }

        $naturezadespesa['codigo'] = $buscand->codigo_nd;
        $naturezadespesa['descricao'] = $this->trataString($buscand->descricao_nd);
        $naturezadespesa['sistema_origem'] = $buscand->sistema_origem;

        return $naturezadespesa;
    }

    /**
     * @OA\Get(
     *      path="/api/empenho/ano/{ano}/ug/{ug}",
     *      operationId="buscaEmpenhoPorAnoUg",
     *      tags={"Empenhos"},
     *      summary="Retorna os empenhos por ano e unidade gestora.",
     *      description="Retorna os empenhos e itens de uma unidade gestora cujo número (do empenho)
     *                    inicie com o ano enviado.",
     * @OA\Parameter(
     *          name="ano",
     *          in="path",
     *          required=true,
     *          description="Ano do empenho",
     *          example="2024",
     *          @OA\Schema(type="integer")
     *      ),
     * @OA\Parameter(
     *          name="ug",
     *          in="path",
     *          required=true,
     *          description="Unidade gestora",
     *          example="123456",
     *          @OA\Schema(type="integer")
     *      ),     *
     * @OA\Response(
     *          response=200,
     *          description="Operação bem-sucedida",
     *          @OA\JsonContent(ref="#/components/schemas/ArrayDeEmpenhosComItens")
     *      ),
     * @OA\Response(
     *          response=401,
     *          description="Error: Unauthorized",
     *          @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *      ),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     * )
     */
    public function buscaEmpenhoPorAnoUg($ano, $ug)
    {
        $retorno = [];

        $empenhos = Empenho::where('ug', $ug)
            ->where('numero', 'LIKE', $ano . 'NE%')
            ->get();

        if (isset($empenhos)) {

            $i = 0;
            foreach ($empenhos as $empenho) {
                $credor = $this->buscaCredor($empenho->favorecido, $empenho->tipofavorecido);
                $ndsubitem = $this->buscaNatureza($empenho->naturezadespesa);
                $planointerno = $this->buscaPlanointerno($empenho->planointerno);

                $empenhodetalhado = new EmpenhodetalhadoController;
                $uggestaoempenho = $empenho->ug . $empenho->gestao . $empenho->numero;

                $retorno[$i]['ug'] = $empenho->ug;
                $retorno[$i]['gestao'] = $empenho->gestao;
                $retorno[$i]['numero'] = $empenho->numero;
                $retorno[$i]['emissao'] = $empenho->emissao;
                $retorno[$i]['ano_emissao'] = $empenho->ano_emissao;
                $retorno[$i]['tipocredor'] = $empenho->tipofavorecido;
                $retorno[$i]['cpfcnpjugidgener'] = $credor['codigo'];
                $retorno[$i]['nome'] = $credor['nome'];
                $retorno[$i]['observacao'] = $this->trataString($empenho->observacao);
                $retorno[$i]['fonte'] = $empenho->fonte;
                $retorno[$i]['ptres'] = $empenho->ptres;
                $retorno[$i]['modalidade_licitacao_siafi'] = $empenho->modalidade_licitacao_siafi;
                $retorno[$i]['sistemaorigem'] = $empenho->sistemaorigem;
                $retorno[$i]['id_sistema_origem'] = $empenho->id_sistema_origem;
                $retorno[$i]['naturezadespesa'] = $ndsubitem['codigo'];
                $retorno[$i]['naturezadespesadescricao'] = $ndsubitem['descricao'];
                $retorno[$i]['nd_sistema_origem'] = $ndsubitem['sistema_origem'];
                $retorno[$i]['picodigo'] = $planointerno['codigo'];
                $retorno[$i]['pidescricao'] = $planointerno['descricao'];
                $retorno[$i]['pi_sistema_origem'] = $planointerno['sistema_origem'];
                $retorno[$i]['itens'] =
                    $empenhodetalhado->buscaEmpenhodetalhadoPorNumeroEmpenho($uggestaoempenho, $ndsubitem['codigo']);
                $i++;
            }
        }

        return response()->json($retorno);
    }


}
