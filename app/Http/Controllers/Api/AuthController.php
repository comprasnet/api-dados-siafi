<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;


class AuthController extends Controller
{

    /**
     *
     * @OA\Post(
     *     tags={"Autenticação"},
     *     summary="Realiza a autenticação no serviço e Receber JWT.",
     *     description="Endpoint utilizado para realizar a autenticação no serviço e Receber JWT.",
     *     path="/api/login",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             required={"username","password"},
     *             @OA\Property(
     *                 property="username",
     *                 type="string",
     *                 maxLength=255,
     *                 description="Usuário que será utilizado para efetuar o login no serviço",
     *                 example="user.teste",
     *             ),
     *             @OA\Property(
     *                 property="password",
     *                 type="string",
     *                 minLength=8,
     *                 maxLength=255,
     *                 description="Senha que será utilizada para efetuar o login no serviço",
     *                 example="senha@123",
     *             ),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Autenticação realizada com sucesso.",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="string",
     *                 description="Status devolvido pela API",
     *                 example="success",
     *             ),
     *             @OA\Property(
     *                 property="authorization",
     *                 type="object",
     *                 @OA\Property(
     *                     property="token",
     *                     type="string",
     *                     description="Token em JWT",
     *                     example="token"
     *                 ),
     *                 @OA\Property(
     *                     property="token_type",
     *                     type="string",
     *                     description="Tipo do token devolvido",
     *                     example="bearer"
     *                 ),
     *                 @OA\Property(
     *                     property="expires_in",
     *                     type="integer",
     *                     description="Tempo de expiração do token em segundos",
     *                     example="3600"
     *                 ),
     *             ),
     *         )
     *    ),
     *    @OA\Response(
     *        response=401,
     *         description="Usuário ou senha incorretos.",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="string",
     *                 description="Status devolvido pela API",
     *                 example="error",
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string",
     *                 description="Mensagem informativa do erro.",
     *                 example="Usuário ou senha incorretos.",
     *             ),
     *         )
     *    ),
     *    @OA\Response(
     *         response=422,
     *         description="Dados enviados não atendem o contrato da API",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="username",
     *                 type="array",
     *                 @OA\Items(
     *                     description="Possivel array contendo mensagem de inconsistencia do username.",
     *                     type="string",
     *                     example="O campo username é obrigatório.",
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="password",
     *                 type="array",
     *                 @OA\Items(
     *                     description="Possivel array contendo mensagem de inconsistencia do password.",
     *                     type="string",
     *                     example={"O campo password é obrigatório.","O campo password deve ter no minimo 8 caracteres."},
     *                 ),
     *             ),
     *         )
     *     ),
     *    @OA\Response(
     *        response=500,
     *        description="ERROR.",
     *    ),
     * )
     */
    /**
     * Faz o login do usuário e retorna um token JWT se as credenciais forem válidas.
     *
     * @return \Illuminate\Http\JsonResponse Retorna uma resposta JSON com o token JWT se as credenciais forem válidas.
     * Se não forem válidas, retorna uma resposta de erro com status 401.
     */
    public function login()
    {
        $credentials = request(['username', 'password']);

        # Faz a autenticação
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Autenticação não autorizada.'], 401);
        }

        # Se o usuário for válido, verifica se é STA ou ADMIN. Se não for, retorna erro.
        $user = auth()->user();
        if ($user->profile !== 'ADMIN' && $user->profile !== 'STA') {
            return response()->json(['error' => 'Autorização inválida.'], 401);
        }

        return $this->respondWithToken($token);
    }

    /***
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * @OA\Post(
     *     path="/api/logout",
     *     summary="Revoga o token de autorização JWT.",
     *     tags={"Autenticação"},
     *     security={{"bearerAuth": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Successfully logged out",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 type="string",
     *                 example="Successfully logged out"
     *             )
     *         )
     *     ),
     *      @OA\Response(
     *           response=401,
     *           description="Error: Unauthorized",
     *           @OA\JsonContent(ref="#/components/schemas/auth_failure")
     *      ),
     * )
     */

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
