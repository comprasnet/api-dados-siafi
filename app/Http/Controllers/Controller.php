<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @OA\Info(
 *     title="API Dados SIAFI",
 *     version="1.0.0",
 * ),
 * @OA\Server(
 *     url=L5_SWAGGER_CONST_HOST,
 *     description="API Server"
 * ),
 * @OA\Tag(
 *     name="Autenticação",
 *     description=""
 * ),
 * @OA\Tag(
 *     name="Órgãos",
 *     description=""
 * ),
 * @OA\Tag(
 *     name="Unidades",
 *     description=""
 * ),
 * @OA\Tag(
 *     name="Natureza de Despesa",
 *     description=""
 * ),
 * @OA\Tag(
 *     name="Empenhos",
 *     description=""
 * ),
 * @OA\Tag(
 *     name="Saldos Contábeis",
 *     description=""
 * ),
  * @OA\Tag(
 *     name="Ordem Bancária",
 *     description=""
 * ),
 * @OA\SecurityScheme(
 *     securityScheme="bearerAuth",
 *     type="http",
 *     scheme="bearer",
 *     bearerFormat="JWT",
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function trataString(string $dado)
    {
        $str = preg_replace('/( )+/', ' ', preg_replace("/[\t\n\r\f\v]/", "", $dado));

        return $str;
    }

    public function retornaDataFormatoMaisOuMenosQtdTipo(string $formato, string $sinal, string $qtd, string $tipo, string $data)
    {
        return date($formato, strtotime($sinal . $qtd . " " . $tipo, strtotime($data)));
    }

    public function buscaDadosUrl($url)
    {
        $context = stream_context_create(array(
            'http' => array(
                'follow_location' => false,
                'timeout' => 2,
            ),
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
            ),
        ));

        return json_decode(file_get_contents($url, true, $context));

    }

    public function getTipoCredor($dado)
    {
        $i = strlen($dado);

        switch ($i) {
            case 14:
                $tipo = '1'; //CNPJ
                break;
            case 11:
                $tipo = '2'; //CPF
                break;
            case 6:
                $tipo = '4'; //UG
                break;
            case 3:
                $tipo = '5'; //AGENCIA BANCO
                break;
            default:
                $tipo = '3'; //ID GENERICO
        }

        return $tipo;
    }

    public function formataCnpjCpfTipo($dado, $tipo)
    {
        $retorno = $dado;

        if ($tipo == '1') {
            $d[0] = substr($dado, 0, 2);
            $d[1] = substr($dado, 2, 3);
            $d[2] = substr($dado, 5, 3);
            $d[3] = substr($dado, 8, 4);
            $d[4] = substr($dado, 12, 2);

            $retorno = $d[0] . '.' . $d[1] . '.' . $d[2] . '/' . $d[3] . '-' . $d[4];

        }

        if ($tipo == '2') {
            $d[0] = substr($dado, 0, 3);
            $d[1] = substr($dado, 3, 3);
            $d[2] = substr($dado, 6, 3);
            $d[3] = substr($dado, 9, 2);

            $retorno = $d[0] . '.' . $d[1] . '.' . $d[2] . '-' . $d[3];
        }

        return $retorno;
    }

    /**
     * @OA\Components(
     *      @OA\Schema(
     *          schema="auth_failure",
     *          type="object",
     *          @OA\Property(property="error",type="string",example="Token de autorização não encontrado."),
     *      ),
     *      @OA\Schema(
     *          schema="orgaos",
     *          type="object",
     *          @OA\Property(
     *              property="orgao_superior",
     *              type="string",
     *              description="Código do órgão 'pai'",
     *              example="20000"
     *          ),
     *          @OA\Property(
     *              property="codigo",
     *              type="string",
     *              description="Código do órgão",
     *              example="20104"
     *          ),
     *          @OA\Property(
     *              property="gestao",
     *              type="string",
     *              description="Código da gestão",
     *              example="00001"
     *          ),
     *          @OA\Property(
     *              property="nome",
     *              type="string",
     *              description="Nome do órgão",
     *              example="SECRETARIA DE ASSUNTOS ESTRATEGICOS"
     *          ),
     *          @OA\Property(
     *              property="cnpj",
     *              type="string",
     *              description="CNPJ do órgão, sem formatação",
     *              example="00394411003710"
     *          ),
     *          @OA\Property(
     *              property="codigo_siorg",
     *              type="integer",
     *              description="Código no SIORG",
     *              example=0
     *          ),
     *          @OA\Property(
     *              property="utiliza_centro_custo",
     *              type="string",
     *              description="Indica se utiliza centro de custo ou não. Os valores podem ser 'Sim' ou 'Não'.",
     *              example=""
     *          ),
     *          @OA\Property(
     *              property="esfera",
     *              type="string",
     *              example="Federal"
     *          ),
     *          @OA\Property(
     *              property="situacao",
     *              type="boolean",
     *              description="Situação do órgão",
     *              example="true"
     *          ),
     *          @OA\Property(
     *              property="id_sistema_origem",
     *              type="string",
     *              description="ID do sistema de origem",
     *              example="SIAFI"
     *          )
     *     ),
     *     @OA\Schema(
     *          schema="orgaossuperiores",
     *          type="object",
     *              @OA\Property(
     *                  property="orgao_superior",
     *                  type="string",
     *                  description="Estará sempre vazio, pois órgão superior não tem órgão 'pai'",
     *                  example=""
     *              ),
     *              @OA\Property(
     *                  property="codigo",
     *                  type="string",
     *                  description="Código do órgão",
     *                  example="12345"
     *              ),
     *              @OA\Property(
     *                  property="gestao",
     *                  type="string",
     *                  description="Código da gestão",
     *                  example="12345"
     *              ),
     *              @OA\Property(
     *                  property="nome",
     *                  type="string",
     *                  description="Nome do órgão superior",
     *                  example="Câmara dos deputados"
     *              ),
     *              @OA\Property (
     *                  property="daas",
     *                  type="boolean",
     *                  description="",
     *                  example="false"
     *              ),
     *              @OA\Property(
     *                  property="cnpj",
     *                  type="string",
     *                  description="CNPJ do órgão, sem formatação",
     *                  example="00123456000178"
     *              ),
     *              @OA\Property(
     *                  property="utiliza_centro_custo",
     *                  type="integer",
     *                  description="Indica se utiliza centro de custo ou não. Os valores podem ser 0 ou 1.",
     *                  example="0"
     *              ),
     *              @OA\Property(
     *                   property="codigo_siorg",
     *                   type="string",
     *                   description="Código no SIORG",
     *                   example="7123456"
     *              ),
     *              @OA\Property(
     *                   property="tipo_administracao",
     *                   type="string",
     *                   description="Tipo de administração.",
     *                   example=""
     *              ),
     *              @OA\Property(
     *                   property="poder",
     *                   type="string",
     *                   description="Tipo de poder",
     *                   example="Executivo"
     *              ),
     *              @OA\Property(
     *                   property="esfera",
     *                   type="string",
     *                   description="Esfera do órgão",
     *                   example="Federal"
     *              ),
     *              @OA\Property(
     *                   property="situacao",
     *                   type="boolean",
     *                   description="Situação do órgão",
     *                   example="true"
     *              ),
     *              @OA\Property(
     *                   property="codigosiasg",
     *                   type="string",
     *                   description="Código do órgão no SIASG",
     *                   example="123456"
     *              ),
     *              @OA\Property(
     *                   property="id_sistema_origem",
     *                   type="string",
     *                   description="Identificador do sistema de origem",
     *                   example="SIAFI"
     *              ),
     *     ),
     *     @OA\Schema(
     *          schema="unidades",
     *          type="object",
     *              @OA\Property(
     *                  property="orgao",
     *                  type="string",
     *                  description="Código do órgão",
     *                  example="123456"
     *              ),
     *              @OA\Property(
     *                  property="codigo",
     *                  type="string",
     *                  description="Código da Unidade",
     *                  example="654321"
     *              ),
     *              @OA\Property(
     *                  property="gestao",
     *                  type="string",
     *                  description="Código da gestão",
     *                  example="123456"
     *              ),
     *              @OA\Property(
     *                  property="funcao",
     *                  type="string",
     *                  description="Executora ou Controle",
     *                  example="Controle"
     *              ),
     *              @OA\Property(
     *                  property="cnpj",
     *                  type="string",
     *                  description="CNPJ da unidade, sem formatação",
     *                  example="00123456000178"
     *              ),
     *              @OA\Property(
     *                  property="nome",
     *                  type="string",
     *                  description="Nome da unidade",
     *                  example="Procuradoria Jurídica"
     *               ),
     *              @OA\Property(
     *                  property="nomeresumido",
     *                  type="string",
     *                  description="Nome resumido da unidade",
     *                  example="Procuradoria"
     *               ),
     *               @OA\Property(
     *                   property="codigo_siorg",
     *                   type="string",
     *                   description="Código no SIORG",
     *                   example="7123456"
     *               ),
     *               @OA\Property(
     *                   property="utiliza_centro_custo",
     *                   type="integer",
     *                   description="Indica se utiliza centro de custo ou não. Os valores podem ser 0 ou 1",
     *                   example="1"
     *               ),
     *               @OA\Property(
     *                   property="situacao",
     *                   type="boolean",
     *                   description="Situação do unidade",
     *                   example="true"
     *               ),
     *               @OA\Property(
     *                   property="id_sistema_origem",
     *                   type="string",
     *                   description="ID do sistema de origem",
     *                   example="SIAFI"
     *               ),
     *     ),
     *     @OA\Schema(
     *         schema="Empenho",
     *         type="object",
     *             @OA\Property(
     *                 property="num_lista",
     *                 type="string",
     *                 description="Código único formado por: Número da UG + Código da Gestão + Número do empenho",
     *                 example="110161000012024NE500001"
     *             ),
     *             @OA\Property(
     *                 property="ug",
     *                 type="string",
     *                 description="Código único da ug, com 6 dígitos",
     *                 example="123456"
     *             ),
     *             @OA\Property(
     *                 property="gestao",
     *                 type="string",
     *                 description="Código único da gestão, com 5 dígitos",
     *                 example="12345"
     *             ),
     *             @OA\Property(
     *                 property="numero",
     *                 type="string",
     *                 description="Número do empenho. Formado por ANO + 'NE' + Número com 6 dígitos",
     *                 example="2023NE123456"
     *             ),
     *             @OA\Property(
     *                 property="emissao",
     *                 type="string",
     *                 format="date",
     *                 description="Data de emissão do empenho",
     *                 example="2023-02-01"
     *              ),
     *              @OA\Property(
     *                  property="ano_emissao",
     *                  type="integer",
     *                  description="Data de emissão do empenho",
     *                  example="2023"
     *              ),
     *              @OA\Property(
     *                  property="tipocredor",
     *                  type="string",
     *                  description="1 para PJ e 2 para PF",
     *                  example="1"
     *              ),
     *              @OA\Property(
     *                  property="cpfcnpjugidgener",
     *                  type="string",
     *                  description="CPF ou CNPJ",
     *                  example="123.456.789-10"
     *              ),
     *              @OA\Property(
     *                  property="nome",
     *                  type="string",
     *                  description="Nome do credor",
     *                  example="José da Silva"
     *              ),
     *              @OA\Property(
     *                  property="observacao",
     *                  type="string",
     *                  description="Observações do empenho",
     *                  example="Contratação de empresa especializada em serviços gerais."
     *              ),
     *              @OA\Property(
     *                  property="fonte",
     *                  type="string",
     *                  description="Código da fonte",
     *                  example="1128123450"
     *              ),
     *              @OA\Property(
     *                  property="modalidade_licitacao_siafi",
     *                  type="string",
     *                  description="Código da modalidade de licitação",
     *                  example="10"
     *              ),
     *              @OA\Property(
     *                  property="ptres",
     *                  type="string",
     *                  description="Código do Programa de Trabalho Resumido",
     *                  example="136457"
     *              ),
     *              @OA\Property(
     *                  property="naturezadespesa",
     *                  type="string",
     *                  description="Código da natureza de despesa",
     *                  example="333039"
     *              ),
     *              @OA\Property(
     *                  property="naturezadespesadescricao",
     *                  type="string",
     *                  description="Descrição da natureza de despesa",
     *                  example="Despesas correntes"
     *              ),
     *              @OA\Property(
     *                  property="nd_sistema_origem",
     *                  type="string",
     *                  description="Sistema de origem da natureza",
     *                  example="SIAFI"
     *              ),
     *              @OA\Property(
     *                  property="picodigo",
     *                  type="string",
     *                  description="Código do Plano Interno",
     *                  example="AGU1234"
     *              ),
     *              @OA\Property(
     *                  property="pidescricao",
     *                  type="string",
     *                  description="Descrição do Plano Interno",
     *                  example="Pagamento de pessoal"
     *              ),
     *              @OA\Property(
     *                  property="pi_sistema_origem",
     *                  type="string",
     *                  description="Sistema de origem do Plano Inerno",
     *                  example="SIAFI"
     *              ),
     *              @OA\Property(
     *                  property="sistemaorigem",
     *                  type="string",
     *                  description="Descrição do sistema de origem do empenho",
     *                  example="SIAFI"
     *              ),
     *              @OA\Property(
     *                  property="id_sistema_origem",
     *                  type="string",
     *                  description="Código/SIGLA do Sistema de origem do empenho",
     *                  example="SIAFI"
     *              ),
     *              @OA\Property(
     *                  property="informacaocomplementar",
     *                  type="string",
     *                  description="Informações complementares do empenho",
     *                  example=""
     *              ),
     *              @OA\Property(
     *                  property="itens",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Item")
     *              )
     *     ),
     *     @OA\Schema(
     *         schema="Item",
     *         type="object",
     *             @OA\Property(
     *                 property="numeroli",
     *                 type="string",
     *                 description="Código único formado por: Número da UG + Código da Gestão + Número do empenho",
     *                 example="110161000012024NE500001"
     *             ),
     *             @OA\Property(
     *                 property="numitem",
     *                 type="string",
     *                 description="Número do item",
     *                 example="001"
     *             ),
     *             @OA\Property(
     *                 property="subitem",
     *                 type="string",
     *                 description="Número do subitem",
     *                 example="02"
     *             ),
     *             @OA\Property(
     *                 property="subitemdescricao",
     *                 type="string",
     *                 description="Descrição do subitem",
     *                 example="Adicional noturno"
     *             ),
     *             @OA\Property(
     *                 property="valorunitario",
     *                 type="number",
     *                 description="Valor unitário",
     *                 example="25.00"
     *             ),
     *             @OA\Property(
     *                 property="quantidade",
     *                 type="number",
     *                 description="Quantidade",
     *                 example="2.00000"
     *             ),
     *             @OA\Property(
     *                  property="valortotal",
     *                  type="number",
     *                  description="Valor total",
     *                  example="50.00"
     *             ),
     *     ),
     *     @OA\Schema(
     *          schema="EmpenhoUnico",
     *          type="object",
     *             @OA\Property(
     *                 property="num_lista",
     *                 type="string",
     *                 description="Código único formado por: Número da UG + Código da Gestão + Número do empenho",
     *                 example="110161000012024NE500001"
     *             ),
     *             @OA\Property(
     *                 property="gestao",
     *                 type="string",
     *                 description="Código único da gestão, com 5 dígitos",
     *                 example="12345"
     *             ),
     *             @OA\Property(
     *                 property="ug",
     *                 type="string",
     *                 description="Código único da ug, com 6 dígitos",
     *                 example="123456"
     *             ),
     *             @OA\Property(
     *                 property="numero",
     *                 type="string",
     *                 description="Número do empenho. Formado por ANO + 'NE' + Número com 6 dígitos",
     *                 example="2023NE123456"
     *             ),
     *             @OA\Property(
     *                 property="emissao",
     *                 type="string",
     *                 format="date",
     *                 description="Data de emissão do empenho",
     *                 example="2023-02-01"
     *              ),
     *              @OA\Property(
     *                  property="ano_emissao",
     *                  type="integer",
     *                  description="Data de emissão do empenho",
     *                  example="2023"
     *              ),
     *              @OA\Property(
     *                  property="tipocredor",
     *                  type="string",
     *                  description="1 para PJ e 2 para PF",
     *                  example="1"
     *              ),
     *              @OA\Property(
     *                  property="cpfcnpjugidgener",
     *                  type="string",
     *                  description="CPF ou CNPJ",
     *                  example="123.456.789-10"
     *              ),
     *              @OA\Property(
     *                  property="nome",
     *                  type="string",
     *                  description="Nome do credor",
     *                  example="José da Silva"
     *              ),
     *              @OA\Property(
     *                  property="observacao",
     *                  type="string",
     *                  description="Observações do empenho",
     *                  example="Contratação de empresa especializada em serviços gerais."
     *              ),
     *              @OA\Property(
     *                  property="fonte",
     *                  type="string",
     *                  description="Código da fonte",
     *                  example="1128123450"
     *              ),
     *              @OA\Property(
     *                  property="modalidade_licitacao_siafi",
     *                  type="string",
     *                  description="Código da modalidade de licitação",
     *                  example="10"
     *              ),
     *              @OA\Property(
     *                  property="ptres",
     *                  type="string",
     *                  description="Código do Programa de Trabalho Resumido",
     *                  example="136457"
     *              ),
     *              @OA\Property(
     *                  property="naturezadespesa",
     *                  type="string",
     *                  description="Código da natureza de despesa",
     *                  example="333039"
     *              ),
     *              @OA\Property(
     *                  property="naturezadespesadescricao",
     *                  type="string",
     *                  description="Descrição da natureza de despesa",
     *                  example="Despesas correntes"
     *              ),
     *              @OA\Property(
     *                  property="picodigo",
     *                  type="string",
     *                  description="Código do Plano Interno",
     *                  example="AGU1234"
     *              ),
     *              @OA\Property(
     *                  property="pidescricao",
     *                  type="string",
     *                  description="Descrição do Plano Interno",
     *                  example="Pagamento de pessoal"
     *              ),
     *              @OA\Property(
     *                  property="sistemaorigem",
     *                  type="string",
     *                  description="Descrição do sistema de origem do empenho",
     *                  example="SIAFI"
     *              ),
     *              @OA\Property(
     *                  property="informacaocomplementar",
     *                  type="string",
     *                  description="Informações complementares do empenho",
     *                  example=""
     *              ),
     *     ),
     *     @OA\Schema (
     *         schema="NaturezasDespesa",
     *         type="object",
     *             @OA\Property(
     *                 property="id",
     *                 type="integer",
     *                 description="ID da natureza de despesa",
     *                 example=47
     *             ),
     *             @OA\Property(
     *                 property="codigo_nd",
     *                 type="string",
     *                 description="Código da natureza de despesa",
     *                 example="333039"
     *             ),
     *             @OA\Property(
     *                 property="descricao_nd",
     *                 type="string",
     *                 description="Descrição da natureza de despesa",
     *                 example="Despesas correntes"
     *             ),
     *             @OA\Property(
     *                 property="codigo_subitem",
     *                 type="string",
     *                 description="Código do subitem",
     *                 example="00"
     *             ),
     *             @OA\Property(
     *                 property="descricao_subitem",
     *                 type="string",
     *                 description="Descrição do subitem",
     *                 example="Transferência a instituições"
     *             ),
     *             @OA\Property(
     *                 property="created_at",
     *                 type="string",
     *                 format="date",
     *                 description="Data de criação da natureza de despesa",
     *                 example="2024-01-31 19:27:29"
     *             ),
     *             @OA\Property(
     *                 property="updated_at",
     *                 type="string",
     *                 format="date",
     *                 description="Data de atualização da natureza de despesa",
     *                 example="2024-01-31 19:27:29"
     *             ),
     *             @OA\Property(
     *                 property="sistema_origem",
     *                 type="string",
     *                 description="Código/SIGLA do sistema de origem",
     *                 example="SIAFI"
     *              )
     *     ),
     *     @OA\Schema(
     *      schema="SaldoContabil",
     *     type="object",
     *      @OA\Property(
     *          property="id",
     *          type="integer",
     *          description="ID do saldo contábil",
     *          example="1"
     *      ),
     *      @OA\Property(
     *          property="unidade",
     *          type="string",
     *          description="Código da unidade",
     *          example="123456"
     *      ),
     *      @OA\Property(
     *          property="gestao",
     *          type="string",
     *          description="Código da gestão",
     *          example="12345"
     *      ),
     *      @OA\Property(
     *          property="conta_contabil",
     *          type="string",
     *          description="Conta contábil",
     *          example="622920103"
     *      ),
     *      @OA\Property(
     *          property="conta_corrente",
     *          type="string",
     *          description="Conta corrente",
     *          example="2024NE12345699"
     *      ),
     *      @OA\Property(
     *          property="debito_inicial",
     *          type="number",
     *          description="Valor inicial de débito",
     *          example="2500.00"
     *      ),
     *      @OA\Property(
     *          property="credito_inicial",
     *          type="number",
     *          description="Valor inicial de crédito",
     *          example="2500.00"
     *      ),
     *      @OA\Property(
     *          property="debito_mensal",
     *          type="number",
     *          description="Débito mensal",
     *          example="2500.00"
     *      ),
     *      @OA\Property(
     *          property="credito_mensal",
     *          type="number",
     *          description="Crédito mensal",
     *          example="2500.00"
     *      ),
     *      @OA\Property(
     *          property="saldo",
     *          type="number",
     *          description="Saldo",
     *          example="2500.00"
     *      ),
     *      @OA\Property(
     *          property="tiposaldo",
     *          type="string",
     *          description="Tipo de saldo (C ou D)",
     *          example="D"
     *      ),
     *      @OA\Property(
     *          property="deleted_at",
     *          type="string",
     *          description="Data de exclusão",
     *          example="null"
     *      ),
     *      @OA\Property(
     *          property="created_at",
     *          type="string",
     *          description="Data de criação",
     *          example="2024-01-31 19:27:29"
     *      ),
     *      @OA\Property(
     *          property="updated_at",
     *          type="string",
     *          description="Data de atualização",
     *          example="2024-01-31 19:27:29"
     *      ),
     *      @OA\Property(
     *          property="ano",
     *          type="string",
     *          description="Ano",
     *          example="2024"
     *      ),
     *      @OA\Property(
     *          property="id_sistema_origem",
     *          type="string",
     *          description="ID do sistema de origem",
     *          example="SIAFI"
     *      )
     *  ),
     *  @OA\Schema(
     *      schema="SaldoContabilUnico",
     *      description="Schema para representar um saldo contábil sem ser array",
     *      title="Saldo Contábil",
     *      required={"id", "unidade", "gestao", "conta_contabil", "conta_corrente", "debito_inicial", "credito_inicial", "debito_mensal", "credito_mensal", "saldo", "tiposaldo", "created_at", "updated_at", "ano", "id_sistema_origem"},
     *      type="object",
     *      @OA\Property(
     *          property="id",
     *          type="integer",
     *          description="ID do saldo contábil",
     *          example=2808538106
     *      ),
     *      @OA\Property(
     *          property="unidade",
     *          type="string",
     *          description="Código da unidade",
     *          example="173057"
     *      ),
     *      @OA\Property(
     *          property="gestao",
     *          type="string",
     *          description="Código da gestão",
     *          example="17804"
     *      ),
     *      @OA\Property(
     *          property="conta_contabil",
     *          type="string",
     *          description="Conta contábil",
     *          example="622920103"
     *      ),
     *      @OA\Property(
     *          property="conta_corrente",
     *          type="string",
     *          description="Conta corrente",
     *          example="2023NE09400299"
     *      ),
     *      @OA\Property(
     *          property="debito_inicial",
     *          type="string",
     *          description="Valor do débito inicial",
     *          example="0.00"
     *      ),
     *      @OA\Property(
     *          property="credito_inicial",
     *          type="string",
     *          description="Valor do crédito inicial",
     *          example="0.00"
     *      ),
     *      @OA\Property(
     *          property="debito_mensal",
     *          type="string",
     *          description="Valor do débito mensal",
     *          example="0.00"
     *      ),
     *      @OA\Property(
     *          property="credito_mensal",
     *          type="string",
     *          description="Valor do crédito mensal",
     *          example="0.00"
     *      ),
     *      @OA\Property(
     *          property="saldo",
     *          type="string",
     *          description="Saldo",
     *          example="5.00"
     *      ),
     *      @OA\Property(
     *          property="tiposaldo",
     *          type="string",
     *          description="Tipo de saldo",
     *          example="C"
     *      ),
     *      @OA\Property(
     *          property="deleted_at",
     *          type="string",
     *          description="Data de exclusão",
     *          nullable=true,
     *          example=null
     *      ),
     *      @OA\Property(
     *          property="created_at",
     *          type="string",
     *          description="Data de criação",
     *          example="2023-12-01 16:20:52"
     *      ),
     *      @OA\Property(
     *          property="updated_at",
     *          type="string",
     *          description="Data de atualização",
     *          example="2023-12-01 16:20:52"
     *      ),
     *      @OA\Property(
     *          property="ano",
     *          type="string",
     *          description="Ano",
     *          example="2023"
     *      ),
     *      @OA\Property(
     *          property="id_sistema_origem",
     *          type="string",
     *          description="Identificador do sistema de origem",
     *          example="SIAFI"
     *      )
     *  ),
     *  @OA\Schema(
     *      schema="Ordembancaria",
     *      type="object",
     *      @OA\Property(
     *          property="ug",
     *          type="string",
     *          description="Código único da ug, com 6 dígitos",
     *          example="123456"
     *      ),
     *      @OA\Property(
     *          property="gestao",
     *          type="string",
     *          description="Código único da gestão, com 5 dígitos",
     *          example="12345"
     *      ),
     *      @OA\Property(
     *          property="numero",
     *          type="string",
     *          description="Número do documento. Formado por ANO + 'CÓDIGO' + Número com 6 dígitos",
     *          example="2023OB123456"
     *      ),
     *      @OA\Property(
     *          property="emissao",
     *          type="string",
     *          format="date",
     *          description="Data de emissão da ordem bancária",
     *          example="2023-02-01"
     *       ),
     *       @OA\Property(
     *          property="tipofavorecido",
     *          type="string",
     *          description="1 para PJ e 2 para PF",
     *          example="2"
     *       ),
     *       @OA\Property(
     *          property="favorecidocodigo",
     *          type="string",
     *          description="Identificador do favorecido",
     *          example="123.456.123-00 ou 00.123.123/0001-00"
     *       ),
     *       @OA\Property(
     *          property="favorecidonome",
     *          type="string",
     *          description="Nome do favorecido",
     *          example="CREDOR XXXXXX"
     *       ),
     *       @OA\Property(
     *          property="observacao",
     *          type="string",
     *          description="Observações da ordem bancária",
     *          example="PAGAMENTO FATURA XXXX"
     *       ),
     *       @OA\Property(
     *          property="tipoob",
     *          type="string",
     *          description="Tipo da ordem bancária",
     *          example="59"
     *       ),
     *       @OA\Property(
     *          property="processo",
     *          type="string",
     *          description="Número do processo",
     *          example="12345.123456/1234-12"
     *       ),
     *       @OA\Property(
     *          property="cancelamentoob",
     *          type="string",
     *          description="Ordem bancária cancelada. 0 para não e 1 para sim.",
     *          example="1"
     *       ),
     *       @OA\Property(
     *          property="numeroobcancelamento",
     *          type="string",
     *          description="Número do documento da ordem bancária que solicitou o cancelamento",
     *          example="123456123452024OB123456"
     *       ),
     *       @OA\Property(
     *          property="valor",
     *          type="string",
     *          description="Valor da ordem bancária",
     *          example="9999.99"
     *       ),
     *       @OA\Property(
     *          property="documentoorigem",
     *          type="string",
     *          description="Número do documento de origem",
     *          example="2024AV123456"
     *       ),
     *       @OA\Property(
     *          property="empenhos",
     *          type="array",
     *          @OA\Items(ref="#/components/schemas/EmpenhoOrdembancaria")
     *       ),
     *       @OA\Property(
     *          property="conta",
     *          type="string",
     *          description="Número da conta bancária",
     *          example="123456789"
     *       ),
     *       @OA\Property(
     *          property="agencia",
     *          type="string",
     *          description="Número da agência bancária",
     *          example="12345"
     *       ),
     *       @OA\Property(
     *          property="banco",
     *          type="string",
     *          description="Número do banco",
     *          example="001"
     *       )
     *  ),
     *  @OA\Schema(   
     *       schema="EmpenhoOrdembancaria",
     *       type="empenhos",
     *       description="Código único formado por: Número da UG + Código da Gestão + Número do empenho",
     *       example="110161000012024NE500001"
     *  )

     * )
     */


    /*************************/
    /*** ARRAYS DE SCHEMAS ***/
    /*************************/

    /**
     * @OA\Schema(
     *     schema="ArrayDeEmpenhosComItens",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/Empenho")
     * )
     */

    /**
     * @OA\Schema(
     *     schema="ArrayDeNaturezasDaDespesa",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/NaturezasDespesa")
     * )
     */

    /**
     * @OA\Schema(
     *     schema="ArraySaldoContabil",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/SaldoContabil")
     * )
     */

    /**
     * @OA\Schema(
     *     schema="ArrayEmpenhosOrdembancaria",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/Ordembancaria")
     * )
     */

}
