<?php

namespace App\Models\Daas\AcessaTG;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SaldosDaas extends Model
{

    public function buscaSaldoContabilCargaUnidadeAno(string $unidade, string $documento_ano)
    {
        $contas_contabeis = implode(',', config('app.saldo_contas_contabeis'));

        $sql = "select
                DISTINCT
                coalesce(pa11.ID_ANO, pa12.ID_ANO)  ano,
                coalesce(pa11.ID_UG_EXEC, pa12.ID_UG_EXEC)  unidade,
                a16.ID_GESTAO  gestao,
                coalesce(pa11.ID_CONTA_CONTABIL, pa12.ID_CONTA_CONTABIL)  conta_contabil,
                a14.NR_CONTA_CORRENTE  conta_corrente,
                '0' AS debito_inicial,
                '0' AS credito_inicial,
                pa12.MOVIMDEVEDORACUMMOEDAORIGEMCO  debito_mensal,
                pa12.MOVIMCREDORACUMMOEDAORIGEMCON  credito_mensal,
                CASE
                    WHEN pa11.SALDORCONTACONTBIL = '0E-9' THEN
                    '0'
                    ELSE
                    pa11.SALDORCONTACONTBIL
                END AS saldo,
                CASE
                  WHEN a17.NO_IN_TP_SALDO_CCON = 'DEVEDOR' THEN
                     'D'
                  ELSE
                     'C'
                END AS tiposaldo
            from	(select	a17.ID_GESTAO_PRIN  ID_GESTAO,
                    a16.ID_UG  ID_UG_EXEC,
                    a11.ID_CONTA_CORRENTE_SK  ID_CONTA_CORRENTE_SK,
                    a15.ID_CONTA_CONTABIL_DESTINO  ID_CONTA_CONTABIL,
                    a11.ID_ANO_LANC  ID_ANO,
                    sum((a11.VA_MOVIMENTO_LIQUIDO * a13.PE_TAXA))  SALDORCONTACONTBIL
                from	DWTG_Colunar_VBL.WF_LANCAMENTO_EP20	a11
                    join	DWTG_Colunar_VBL.WD_MOEDA	a12
                      on 	(a11.ID_MOEDA_UG_EXEC_H = a12.ID_MOEDA)
                    join	DWTG_Colunar_VBL.WD_TAXA_CAMBIO_MENSAL	a13
                      on 	(a12.ID_MOEDA = a13.ID_MOEDA_ORIGEM)
                    join	DWTG_Colunar_VBL.WA_MES_ACUM_SEM_ANO	a14
                      on 	(a11.ID_MES_LANC = a14.ID_MES_ACUM_ANO_SALDO)
                    join	DWTG_Colunar_VBL.WD_CONTA_CONTABIL_EXERCICIO	a15
                      on 	(a11.ID_ANO_LANC = a15.ID_ANO and
                    a11.ID_CONTA_CONTABIL_LANC = a15.ID_CONTA_CONTABIL)
                    join	DWTG_Colunar_VBL.WD_UG_EXERCICIO	a16
                      on 	(a11.ID_ANO_LANC = a16.ID_ANO and
                    a11.ID_UG_EXEC = a16.ID_UG)
                    join	DWTG_Colunar_VBL.WD_ORGAO	a17
                      on 	(a16.ID_ORGAO_UG = a17.ID_ORGAO)
                    join	DWTG_Colunar_VBL.WD_CONTA_CONTABIL	a18
                      on 	(a15.ID_CONTA_CONTABIL_DESTINO = a18.ID_CONTA_CONTABIL)
                where	(a11.ID_UG_EXEC = '$unidade'
                 and a11.ID_ANO_LANC = '$documento_ano'
                 and a18.ID_CONTA_CONTABIL in ($contas_contabeis)
                 and a13.ID_ANO = a11.ID_ANO_LANC
                 and a13.ID_MES = a14.ID_MES)
                group by	a17.ID_GESTAO_PRIN,
                    a16.ID_UG,
                    a11.ID_CONTA_CORRENTE_SK,
                    a15.ID_CONTA_CONTABIL_DESTINO,
                    a11.ID_ANO_LANC
                )	pa11
                full outer join	(select	a14.ID_GESTAO_PRIN  ID_GESTAO,
                    a13.ID_UG  ID_UG_EXEC,
                    a11.ID_CONTA_CORRENTE_SK  ID_CONTA_CORRENTE_SK,
                    a12.ID_CONTA_CONTABIL_DESTINO  ID_CONTA_CONTABIL,
                    a11.ID_ANO_LANC  ID_ANO,
                    sum(a11.VA_CREDITO)  MOVIMCREDORACUMMOEDAORIGEMCON,
                    sum(a11.VA_DEBITO)  MOVIMDEVEDORACUMMOEDAORIGEMCO
                from	DWTG_Colunar_VBL.WF_LANCAMENTO_EP20	a11
                    join	DWTG_Colunar_VBL.WD_CONTA_CONTABIL_EXERCICIO	a12
                      on 	(a11.ID_ANO_LANC = a12.ID_ANO and
                    a11.ID_CONTA_CONTABIL_LANC = a12.ID_CONTA_CONTABIL)
                    join	DWTG_Colunar_VBL.WD_UG_EXERCICIO	a13
                      on 	(a11.ID_ANO_LANC = a13.ID_ANO and
                    a11.ID_UG_EXEC = a13.ID_UG)
                    join	DWTG_Colunar_VBL.WD_ORGAO	a14
                      on 	(a13.ID_ORGAO_UG = a14.ID_ORGAO)
                    join	DWTG_Colunar_VBL.WD_CONTA_CONTABIL	a15
                      on 	(a12.ID_CONTA_CONTABIL_DESTINO = a15.ID_CONTA_CONTABIL)
                where	(a11.ID_UG_EXEC = '$unidade'
                 and a11.ID_ANO_LANC = '$documento_ano'
                 and a15.ID_CONTA_CONTABIL in ($contas_contabeis))
                group by	a14.ID_GESTAO_PRIN,
                    a13.ID_UG,
                    a11.ID_CONTA_CORRENTE_SK,
                    a12.ID_CONTA_CONTABIL_DESTINO,
                    a11.ID_ANO_LANC
                )	pa12
                  on 	(pa11.ID_ANO = pa12.ID_ANO and
                pa11.ID_CONTA_CONTABIL = pa12.ID_CONTA_CONTABIL and
                pa11.ID_CONTA_CORRENTE_SK = pa12.ID_CONTA_CORRENTE_SK and
                pa11.ID_GESTAO = pa12.ID_GESTAO and
                pa11.ID_UG_EXEC = pa12.ID_UG_EXEC)
                join	DWTG_Colunar_VBL.WD_CONTA_CONTABIL	a13
                  on 	(coalesce(pa11.ID_CONTA_CONTABIL, pa12.ID_CONTA_CONTABIL) = a13.ID_CONTA_CONTABIL)
                join	DWTG_Colunar_VBL.WD_CONTA_CORRENTE	a14
                  on 	(coalesce(pa11.ID_CONTA_CORRENTE_SK, pa12.ID_CONTA_CORRENTE_SK) = a14.ID_CONTA_CORRENTE_SK)
                join	DWTG_Colunar_VBL.WD_UG	a15
                  on 	(coalesce(pa11.ID_UG_EXEC, pa12.ID_UG_EXEC) = a15.ID_UG)
                join	DWTG_Colunar_VBL.WD_GESTAO	a16
                  on 	(coalesce(pa11.ID_GESTAO, pa12.ID_GESTAO) = a16.ID_GESTAO)
                join	DWTG_Colunar_VBL.WD_IN_TP_SALDO_CCON	a17
                  on 	(a13.ID_IN_TP_SALDO_CCON = a17.ID_IN_TP_SALDO_CCON)";

        return DB::connection('odbc-dwtg')
            ->select($sql);

    }


}
