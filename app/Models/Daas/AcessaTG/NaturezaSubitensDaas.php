<?php

namespace App\Models\Daas\AcessaTG;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NaturezaSubitensDaas extends Model
{
    public function buscaNaturezaSubitens()
    {
        $sql = "select
                    max(a12.CO_NATUREZA_DESPESA)  codigo_nd,
                    max(a12.NO_NATUREZA_DESPESA)  descricao_nd,
                    a11.ID_SUBITEM_NADE  codigo_subitem,
                    max(a11.NO_NATUREZA_DESPESA_DETA)  descricao_subitem
                from	DWTG_Colunar_VBL.WD_NATUREZA_DESPESA_DETA	a11
                    join	DWTG_Colunar_VBL.WD_NATUREZA_DESPESA	a12
                      on 	(a11.ID_CATEGORIA_ECONOMICA_NADE = a12.ID_CATEGORIA_ECONOMICA_NADE and
                    a11.ID_ELEMENTO_DESPESA_NADE = a12.ID_ELEMENTO_DESPESA_NADE and
                    a11.ID_GRUPO_DESPESA_NADE = a12.ID_GRUPO_DESPESA_NADE and
                    a11.ID_MOAP_NADE = a12.ID_MOAP_NADE)
                where	a11.ID_CATEGORIA_ECONOMICA_NADE in (3, 4) AND
                a11.ID_SUBITEM_NADE >= 0 AND
                a11.NO_NATUREZA_DESPESA_DETA <> 'CODIGO INEXISTENTE NO SIAFI' AND
                a11.NO_NATUREZA_DESPESA_DETA <> '-7' AND
                a11.NO_NATUREZA_DESPESA_DETA <> 'NAO SE APLICA'
                group by	a11.ID_CATEGORIA_ECONOMICA_NADE,
                    a11.ID_GRUPO_DESPESA_NADE,
                    a11.ID_MOAP_NADE,
                    a11.ID_ELEMENTO_DESPESA_NADE,
                    a11.ID_CATEGORIA_ECONOMICA_NADE,
                    a11.ID_GRUPO_DESPESA_NADE,
                    a11.ID_MOAP_NADE,
                    a11.ID_ELEMENTO_DESPESA_NADE,
                    a11.ID_SUBITEM_NADE";

        return DB::connection('odbc-dwtg')
            ->select($sql);
    }
}
