<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quantidadeextracao extends Model
{
    protected $table = 'quantidadeextracao';

    protected $fillable = [
        'dado',
        'quantidade',
    ];

    public function inserirDado(array $dado)
    {
        $contagem = $this->fill($dado);
        $contagem->save();

        return $contagem;
    }

}
