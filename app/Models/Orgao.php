<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orgao extends Model
{
    protected $table = 'orgao';

    protected $hidden = [
        "id",
        'created_at',
        'updated_at'
    ];


    public function buscaTodosOrgaos()
    {

        $retorno = [];
        $orgaos = $this->all();

        foreach ($orgaos as $orgao) {
            $cnpj = '';
            if ($orgao->cnpj != null or $orgao->cnpj != 0) {
                $cnpj = str_pad($orgao->cnpj, 14, "0", STR_PAD_LEFT);
            }

            $retorno[] = [
                "orgao_superior" => ($orgao->orgao_superior == '') ? $orgao->codigo : $orgao->orgao_superior,
                "codigo" => $orgao->codigo,
                "gestao" => $orgao->gestao,
                "nome" => strtoupper($orgao->nome),
                "cnpj" => $cnpj,
                'codigo_siorg' => (int) $orgao->codigo_siorg,
                'utiliza_centro_custo' => (strlen(trim($orgao->utiliza_centro_custo)) > 0) ? substr(trim($orgao->utiliza_centro_custo),0,1) : '',
                'situacao' => $orgao->situacao,
                'esfera' => $orgao->esfera,
                'id_sistema_origem' => $orgao->id_sistema_origem
               
            ];
        }

        return $retorno;

    }


}
