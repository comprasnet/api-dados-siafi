<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class SaldoContabil extends Model
{
    use SoftDeletes;

    protected $table = 'saldoscontabeis';

    protected $fillable = [
        'unidade',
        'gestao',
        'conta_contabil',
        'conta_corrente',
        'debito_inicial',
        'credito_inicial',
        'debito_mensal',
        'credito_mensal',
        'saldo',
        'tiposaldo',
    ];

    public function buscaSaldoContabil(string $unidade, string $gestao, string $conta_contabil, string $conta_corrente)
    {
        $busca = $this->where('unidade', $unidade)
            ->where('gestao', $gestao)
            ->where('conta_contabil', $conta_contabil)
            ->where('conta_corrente', $conta_corrente);

        return $busca->first();
    }

    public function buscaSaldoContabilAno(string $ano, string $unidade, string $gestao, string $conta_contabil, string $conta_corrente)
    {
        $busca = $this->where('ano', $ano)
            ->where('unidade', $unidade)
            ->where(function ($query) use ($gestao) {
                $query->where(function ($query) use ($gestao) {
                    $query->where('id_sistema_origem', 'SIAFI')
                        ->where('gestao', $gestao);
                })
                    ->orWhere('id_sistema_origem', '<>', 'SIAFI');
            })
            ->where('conta_contabil', $conta_contabil)
            ->where('conta_corrente', $conta_corrente);

        return $busca->first();
    }

    public function buscaSaldoPorAnoUnidadeGestaoContasContabeisContacorrente(
        string $ano,
        string $unidade,
        string $gestao,
        array  $contascontabeis,
        string $conta_corrente
    )
    {
        return $this->where('ano', $ano)
            ->where('unidade', $unidade)
            ->where(function ($query) use ($gestao) {
                $query->where(function ($query) use ($gestao) {
                    $query->where('id_sistema_origem', 'SIAFI')
                        ->where('gestao', $gestao);
                })
                    ->orWhere('id_sistema_origem', '<>', 'SIAFI');
            })
            ->whereIn('conta_contabil', $contascontabeis)
            ->where('conta_corrente', $conta_corrente)
            ->get();
    }

    public function buscaSaldoPorUgContaContabilAno(string $ano, string $unidade, string $gestao, string $conta_contabil)
    {
        return $this->where('ano', $ano)
            ->where('unidade', $unidade)
            ->where('conta_contabil', $conta_contabil)
            ->get();

    }

    public function buscaSaldoPorUgAno(string $ano, string $unidade, string $gestao)
    {
        return $this->where('ano', $ano)
            ->where('unidade', $unidade)
            ->where(function ($query) use ($gestao) {
                $query->where(function ($query) use ($gestao) {
                    $query->where('id_sistema_origem', 'SIAFI')
                        ->where('gestao', $gestao);
                })
                    ->orWhere('id_sistema_origem', '<>', 'SIAFI');
            })
            ->orderBy('conta_contabil')
            ->orderBy('conta_corrente')
            ->get();

    }

    public function buscaSaldoPorOrgaoAno(string $ano, string $orgao)
    {

        return DB::table('saldoscontabeis')
            ->join('unidades', 'unidades.codigo', '=', 'saldoscontabeis.unidade')
            ->join('orgao', 'orgao.codigo', '=', 'unidades.orgao')
            ->select([
                'saldoscontabeis.unidade',
                'saldoscontabeis.gestao',
                'saldoscontabeis.conta_contabil',
                'saldoscontabeis.conta_corrente',
                'saldoscontabeis.conta_corrente',
                "saldoscontabeis.debito_inicial",
                "saldoscontabeis.credito_inicial",
                "saldoscontabeis.debito_mensal",
                "saldoscontabeis.credito_mensal",
                "saldoscontabeis.saldo",
                "saldoscontabeis.tiposaldo",
                "saldoscontabeis.created_at",
                "saldoscontabeis.updated_at",
                "saldoscontabeis.ano",
            ])
            ->where('saldoscontabeis.ano', $ano)
            ->where('orgao.codigo', $orgao)
            ->get();
    }


}
