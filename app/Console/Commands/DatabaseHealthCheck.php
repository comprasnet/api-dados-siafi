<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DatabaseHealthCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'health:check-database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica status de conexão com o banco de dados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Log::info('Início de teste de conexão.');
            DB::connection()->getPdo();
            if(DB::connection()->getDatabaseName()){
                Log::info('Conexão com banco de dados feita com sucesso.');
            }
        } catch (\Exception $e) {
            Log::error('Não foi possível conectar com a base de dados. Erro: ' . $e->getMessage());
        }

        return 0;
    }
}
