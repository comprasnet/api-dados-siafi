<?php

namespace App\Jobs;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Api\v1\LeituraArquivosDaasController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LeituraArquivosDaas implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 3200;
    protected $nome;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $nome)
    {
        $this->nome = $nome;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $base = new BaseController();
        $path = config('app.path_pendentes');
        $path_processados = config('app.path_processados');
        $ug = '';

        $nome_arquivo = $path . $this->nome['nomearquivo'];
        $nome_arquivo_destino = $path_processados . $this->nome['nomearquivo'];

        $array_nome = explode('_', $this->nome['nomearquivo']);
        $tipo_arquivo = $array_nome[0];
        if ($array_nome[1] == 'carga' or $array_nome[1] == 'd1') {
            $tipo_extracao = $array_nome[1];
        } else {
            $tipo_extracao = 'all';
        }

        if($tipo_arquivo == 'restosapagar'){
            $ug = $array_nome[2];
        }

        $arquivo = fopen($nome_arquivo.'.txt', 'r');
        $i =0;
        $dado = [];
        while (!feof($arquivo)) {
            $linha = fgets($arquivo, 1024);
            $value = [];
            if($i==0){
                $keys = explode(env('DAAS_DB_DELIMITER1'),$linha);
            }else{
                $value = explode(env('DAAS_DB_DELIMITER1'),$linha);
            }

            if(count($value)) {
                $dados = [];
                foreach($value as $key => $value)
                {
                    $k = trim($keys[$key]);
                    $dados[$k] = trim($value);
                }
                $dado[] = $dados;
            }

            $i++;
        }

        fclose($arquivo);

        if(count($dado)){
            $processa_array_dados = new LeituraArquivosDaasController();
            $retorno = $processa_array_dados->processaDadosArquivoDaas($dado, $tipo_arquivo, $tipo_extracao, $ug);
        }

        $base->moverArquivoProcessado($nome_arquivo_destino,$nome_arquivo,'.txt');

    }
}
