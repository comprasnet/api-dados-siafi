<?php

namespace App\Jobs;

use App\Http\Controllers\Api\v1\AcessaTGDaasController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessaCargaRestosAPagarDaas implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 900;
    protected $unidade;
    protected $ano;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $unidade, string $ano)
    {
        $this->unidade = $unidade;
        $this->ano = $ano;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $processo = new AcessaTGDaasController();
//        $processo->empenhosCargaUnidadeAno($this->unidade,$this->ano);
        $processo->execJdbcClass('cargarestosapagar', $this->unidade, $this->ano);
    }
}
