<?php

namespace App\Jobs;

use App\Http\Controllers\Api\CredorController;
use App\Models\Credor;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AtualizaCredorSicafJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $credores;
    protected $sicaf;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $credores)
    {
        $this->credores = $credores;
        $this->sicaf = new CredorController();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //jogar para um jobs para processar os 5000 fornecedores
        foreach ($this->credores as $credor) {
            $dados = $this->sicaf->buscarCredorSicaf($credor['cpf_cnpj_idgener']);
            if ($dados) {
                $novo_credor = Credor::where('cpf_cnpj_idgener',$credor['cpf_cnpj_idgener'])
                    ->update(['nome' => $dados->nome]);
            }
        }
    }
}
