<?php

namespace App\Integrations;

use GuzzleHttp\Client;

class SICAF
{

    private $url;
    private $username;
    private $password;
    private $env;

    private $client;

    private $access_token;
    private $scope;
    private $token_type;
    private $expires_in;

    public function __construct()
    {
        $this->url = config('app.SICAF_HOST');
        $this->username = config('app.SICAF_USUARIO');
        $this->password = config('app.SICAF_SENHA');
        $this->env = config('app.SICAF_ENV');

        $this->client = new Client([
            'verify' => false,
        ]);
    }

    /**
     * Autentica ao sicaf e retorna o token
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function token()
    {
        $credentials = base64_encode("{$this->username}:{$this->password}");

        $response = $this->client->request('POST', "{$this->url}/token", [
            'headers' => [
                'Authorization' => "Basic $credentials",
            ],
            'form_params' => [
                'grant_type' => 'client_credentials',
            ]
        ]);

        $response = json_decode($response->getBody()->getContents());

        $this->access_token = $response->access_token;
        $this->scope = $response->scope;
        $this->token_type = $response->token_type;
        $this->expires_in = $response->expires_in;

        return $response;
    }

    /**
     * Retorna os dados de situação dos níveis de Credenciamento (I a VI) de um fornecedor nacional
     * @param $cnpj
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function dadosSituacaoNivel($cnpj)
    {
        $this->token();

        $response = $this->client->request('GET', "{$this->url}/{$this->env}/v1/dadossituacaonivel/{$cnpj}", [
            'headers' => [
                'Authorization' => "Bearer {$this->access_token}",
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }

    /**
     * Retorna os dados de situação dos níveis de Credenciamento (I a VI) de um fornecedor nacional
     * @param $cnpj
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function dadosBasicos($cnpj)
    {
        $this->token();

        try {
            $response = $this->client->request('GET', "{$this->url}/{$this->env}/v1/dadosbasicos/{$cnpj}", [
                'headers' => [
                    'Authorization' => "Bearer {$this->access_token}",
                ]
            ]);
        } catch (\Exception $e) {
            return null;
        }

        return json_decode($response->getBody()->getContents());
    }

    public function getAccessToken()
    {
        return $this->access_token;
    }

}
