<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group([
    'namespace' => 'Api',
], function () {
    //API leitura de arquivos qware (STA)
    Route::middleware('jwt.verify')->group(function () {

    });

    # Unidades
    Route::get('/estrutura/unidades', 'EstruturaController@buscaUnidades');

    # Órgãos
    Route::get('/estrutura/orgaos', 'EstruturaController@buscaOrgaos');

    # Órgãos superiores
    Route::get('/estrutura/orgaossuperiores', 'EstruturaController@buscaOrgaosSuperiores');

    # Naturezas de despesas
    Route::get('/estrutura/naturezadespesas', 'EstruturaController@buscaNaturezadespesas');

    # Unidades com empenhos
    Route::get('/unidade/empenho/{ano}/{data?}', 'UnidadeController@getUnidadesComEmpenhos');
    Route::get('/unidade/rp/{ano}/{data?}', 'UnidadeController@getUnidadesComRP');

    # Empenhos
    Route::get('/empenho/ano/{ano}/ug/{ug}/dia/{data?}', 'EmpenhoController@buscaEmpenhoPorDiaUg');
    Route::get('/empenho/{dado}', 'EmpenhoController@buscaEmpenhoPorNumero');
    Route::get('/empenho/dadosbasicos/{dado}', 'EmpenhoController@buscaEmpenhoPorNumeroDadosBasicos');
    Route::get('/empenho/ano/{ano}/ug/{ug}/', 'EmpenhoController@buscaEmpenhoPorAnoUg');

    # Restos
    Route::get('/rp/ano/{ano}/ug/{ug}/', 'RpController@buscaRpPorUg');
    Route::get('/rp/ano/{ano}/ug/{ug}/dia/{data?}', 'RpController@buscaRpPorUg');

    # Empenho detalhado
    Route::get('/empenhodetalhado/{dado}', 'EmpenhodetalhadoController@buscaEmpenhodetalhadoPorNumeroEmpenho');

    # Saldos
    Route::post('/saldocontabil',
        'SaldosContabeisController@retornaSaldoPorAnoUnidadeGestaoContasContabeisContacorrente');
    Route::get('/saldocontabil/ano/{ano}/ug/{unidade}/gestao/{gestao}/contacontabil/{conta_contabil}/contacorrente/{conta_corrente}',
        'SaldosContabeisController@retornaSaldoPorUGGestaoContacontabilContacorrenteAno');
    Route::get('/saldocontabil/ano/{ano}/ug/{unidade}/gestao/{gestao}/contacontabil/{conta_contabil}',
        'SaldosContabeisController@retornaSaldoPorUGGestaoContacontabilAno');
    Route::get('/saldocontabil/ano/{ano}/ug/{unidade}/gestao/{gestao}',
        'SaldosContabeisController@retornaSaldoPorUGGestaoAno');
    Route::get('/saldocontabil/ano/{ano}/orgao/{orgao}',
        'SaldosContabeisController@retornaSaldoPorOrgaoAno');

    Route::get('/ordembancaria/favorecido/{dado}', 'OrdembancariaController@buscaOrdembancariaPorCnpj');
    Route::get('/ordembancaria/empenho/{dado}', 'OrdembancariaController@buscaOrdembancariaPorEmpenho');
    Route::get('/ordembancaria/documento/{dado}', 'OrdembancariaController@buscaOrdembancariaPorDocumento');
    Route::post('/ordembancaria/documentolote', 'OrdembancariaController@buscaOrdembancariaPorLoteDocumento');
    Route::get('/ordembancaria/ano/{ano}/ug/{ug}', 'OrdembancariaController@buscaOrdembancariaPorAnoUg');
    Route::get('/centrocusto/mesref/{mesref}/ug/{ug}', 'CentroCustoController@buscaCentroCustoPorMesrefUg');


//    Route::get('/ler/unidade', 'UnidadeController@ler');
//    Route::get('/ler/orgao', 'OrgaoController@ler');
//    Route::get('/ler/credor', 'CredorController@ler');
//    Route::get('/ler/planointerno', 'PlanointernoController@ler');
//    Route::get('/ler/empenho', 'EmpenhoController@ler');
//    Route::get('/ler/empenhodetalhado', 'EmpenhodetalhadoController@ler');
//    Route::get('/ler/rp', 'RpController@ler');
//    Route::get('/ler/ordembancaria/', 'OrdembancariaController@ler');
//    Route::get('/ler/dochabil', 'DocHabilController@ler');
//    Route::get('/ler/saldocontabil', 'SaldosContabeisController@ler');

//    Route::get('/daas/empenhos/d1/{unidade}/{documento_ano}', 'v1\AcessaTGDaasController@empenhosD1UnidadeAno');
//    Route::get('/daas/empenhos/carga/{unidade}/{documento_ano}', 'v1\AcessaTGDaasController@empenhosCargaUnidadeAno');
//
//    Route::get('/daas/itensempenhos/d1/{unidade}/{documento_ano}', 'v1\AcessaTGDaasController@itensEmpenhosD1UnidadeAno');
//    Route::get('/daas/itensempenhos/carga/{unidade}/{documento_ano}/{meses}', 'v1\AcessaTGDaasController@itensEmpenhosCargaUnidadeAno');
//
//    Route::get('/daas/saldocontabil/d1/{unidade}/{documento_ano}', 'v1\AcessaTGDaasController@saldoContabilD1UnidadeAno');
//    Route::get('/daas/saldocontabil/carga/{unidade}/{documento_ano}', 'v1\AcessaTGDaasController@saldoContabilCargaUnidadeAno');
//
//    Route::get('/daas/orgaos/carga', 'v1\AcessaTGDaasController@orgaos');
//    Route::get('/daas/unidades/carga', 'v1\AcessaTGDaasController@unidades');
//    Route::get('/daas/naturezasubitens/carga', 'v1\AcessaTGDaasController@naturezaSubitens');
//
//    Route::get('/jdbc/executaclasse/{tipo}', 'v1\AcessaTGDaasController@execJdbcClass');

    Route::get('/contrato/unidades', 'v1\ContratosCConController@buscaUnidades');

//    Route::get('/leitura/arquivos_daas', 'v1\ContratosCConController@criaJobsLeituraArquivos');
//    Route::get('/leitura/arquivos_daas_url', 'v1\ContratosCConController@executaLeituraArquivosViaUrl');

    Route::get('/efdreinf/ug/{unidade}/gestao/{gestao}/competencia/{competencia}', 'v1\EfdreinfController@buscarDeducoesPorCompetencia');

//    Route::get('/sicaf/{cnpj}','CredorController@buscarCredorSicaf');
//    Route::get('/sicaf/executa','CredorController@atualizaCredoresComDadosSicaf');


    Route::post('login', 'AuthController@login');
    Route::group([
        'middleware' => ['api', 'jwt.verify'],
    ], function ($router) {
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
    });
});


