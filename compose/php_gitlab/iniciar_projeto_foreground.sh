#!/bin/sh

chmod -R ugo+rw /var/www/html/api-dados-siafi/storage
chmod -R ugo+rw /var/www/html/api-dados-siafi/bootstrap/cache
chown -R www-data:www-data /var/www/html/api-dados-siafi/

rm -rf ./storage/app/public

php -f /var/www/html/api-dados-siafi/artisan key:generate
php -f /var/www/html/api-dados-siafi/artisan migrate --force
php -f /var/www/html/api-dados-siafi/artisan storage:link
php -f /var/www/html/api-dados-siafi/artisan optimize:clear
php -f /var/www/html/api-dados-siafi/artisan cache:clear
php -f /var/www/html/api-dados-siafi/artisan config:cache
php -f /var/www/html/api-dados-siafi/artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
php -f /var/www/html/api-dados-siafi/artisan l5-swagger:generate

service cron start

/usr/sbin/apache2ctl -D FOREGROUND
