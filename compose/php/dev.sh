if [ ! -f ".env" ]; then
  echo 'Criando arquivo .env ...'
  cp .env.example .env
fi
echo 'Baixando dependencias e criando diretório vendor ...'
composer install
composer dump-autoload
echo 'Gerando chave ...'
php artisan key:generate
echo 'Executando migrate ...'
php artisan migrate
echo 'Estabelecendo link simbólico...'
[ -L /var/www/html/contratos/public/storage ] && echo "Link simbólico já existe" || php artisan storage:link
echo 'Criando chave JWT'
php artisan jwt:secret --force
echo 'Iniciando aplicação ...'