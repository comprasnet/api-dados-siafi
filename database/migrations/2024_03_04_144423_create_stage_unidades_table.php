<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stage_unidades', function (Blueprint $table) {
            $table->string('codigo')->nullable();
            $table->string('cnpj')->nullable();
            $table->string('funcao')->nullable();
            $table->string('nome')->nullable();
            $table->string('uf')->nullable();
            $table->string('orgao')->nullable();
            $table->string('nomeresumido')->nullable();
            $table->boolean('daas')->nullable();
            $table->string('id_sistema_origem')->nullable();
            $table->string('codigo_siorg')->nullable();
            $table->boolean('sisg')->nullable();
            $table->boolean('aderiu_siasg')->nullable();
            $table->integer('codigo_municipio_siasg')->nullable();
            $table->boolean('situacao')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stage_unidades');
    }
}
