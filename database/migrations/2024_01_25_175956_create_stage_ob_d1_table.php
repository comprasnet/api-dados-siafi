<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageObD1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('stage_ob_d1')) {
            Schema::create('stage_ob_d1', function (Blueprint $table) {
                $table->string("ug", 4000)->nullable();
                $table->string("gestao", 4000)->nullable();
                $table->string("numero", 4000)->nullable();
                $table->timestamp("emissao")->nullable();
                $table->string("tipofavorecido", 4000)->nullable();
                $table->string("favorecido", 14)->nullable();
                $table->string("bancodestino", 3)->nullable();
                $table->string("agenciadestino", 4)->nullable();
                $table->string("contadestino", 20)->nullable();
                $table->string("processo", 20)->nullable();
                $table->smallInteger("tipoob")->nullable();
                $table->string("observacao", 468)->nullable();
                $table->string("cancelamentoob", 4000)->nullable();
                $table->string("numeroobcancelamento", 4000)->nullable();
                $table->decimal("valor", 17, 2)->nullable();
                $table->string("documentoorigem", 4000)->nullable();
                $table->string("empenho", 4000)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stage_ob_d1');
    }
}
