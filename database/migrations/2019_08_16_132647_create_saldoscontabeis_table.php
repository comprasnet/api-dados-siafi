<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaldoscontabeisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saldoscontabeis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unidade');
            $table->string('gestao');
            $table->string('conta_contabil');
            $table->string('conta_corrente');
            $table->decimal('debito_inicial', 17, 2)->default(0);
            $table->decimal('credito_inicial', 17, 2)->default(0);
            $table->decimal('debito_mensal', 17, 2)->default(0);
            $table->decimal('credito_mensal', 17, 2)->default(0);
            $table->decimal('saldo', 17, 2)->default(0);
            $table->string('tiposaldo')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->unique([
                'unidade',
                'gestao',
                'conta_contabil',
                'conta_corrente'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saldoscontabeis');
    }
}
