<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterTypeOfColumnProfileUsersTable extends Migration
{
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('profile')->default(null)->nullable()->change();
        });
    }

    public function down(): void
    {
        # Utilizado statement para forçar o campo a ser char
        DB::statement("ALTER TABLE users ALTER COLUMN profile TYPE CHAR(1);");
    }
}
