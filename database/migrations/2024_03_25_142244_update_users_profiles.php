<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateUsersProfiles extends Migration
{
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            // Alterar profiles de "A" para "ADMIN"
            DB::table('users')->where('profile', 'A')->update(['profile' => 'ADMIN']);

            // Alterar profiles de "S" para "JULIUS"
            DB::table('users')->where('profile', 'S')->update(['profile' => 'JULIUS']);
        });
    }

    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            // Reverter profiles de "ADMIN" para "A"
            DB::table('users')->where('profile', 'ADMIN')->update(['profile' => 'A']);

            // Reverter profiles de "JULIUS" para "S"
            DB::table('users')->where('profile', 'JULIUS')->update(['profile' => 'S']);
        });
    }
}
