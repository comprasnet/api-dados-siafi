<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('nonce')) {
            Schema::create('nonce', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('user_id');
                $table->string('nonce', 100);
                $table->nullableMorphs('nonceable');
                $table->string('ip', 100);
                $table->boolean('status')->default(TRUE);
                $table->string('funcionalidade');
                $table->text('json_request');
                $table->text('json_response');
                $table->timestamps();
                $table->unique(['user_id','nonce']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('nonce')) {
            Schema::dropIfExists('nonce');
        }    
    }
}
