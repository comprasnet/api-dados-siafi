<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSequencialOperacaoToEmpenhodetalhadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empenhodetalhado', function (Blueprint $table) {
            if (!Schema::hasColumn('empenhodetalhado', 'sequencial_operacao')) {
                $table->string('sequencial_operacao')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhodetalhado', function (Blueprint $table) {
            if (Schema::hasColumn('empenhodetalhado', 'sequencial_operacao')) {
                $table->dropColumn('sequencial_operacao');
            }
        });
    }
}
