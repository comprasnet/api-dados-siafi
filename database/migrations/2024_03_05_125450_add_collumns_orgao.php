<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnsOrgao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orgao', function (Blueprint $table) {
            $table->string('tipo_administracao')->nullable();
            $table->string('poder')->nullable();
            $table->string('esfera')->nullable();
            $table->boolean('situacao')->nullable();
            $table->string('codigosiasg')->nullable();
            $table->string('id_sistema_origem')->nullable()->default('SIAFI');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orgao', function (Blueprint $table) {
            $table->dropColumn('tipo_administracao');
            $table->dropColumn('poder');
            $table->dropColumn('esfera');
            $table->dropColumn('situacao');
            $table->dropColumn('codigosiasg');
            $table->dropColumn('id_sistema_origem');
        });
    }
}
