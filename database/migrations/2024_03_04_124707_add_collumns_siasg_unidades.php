<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnsSiasgUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unidades', function (Blueprint $table) {
            $table->boolean('sisg')->nullable();
            $table->boolean('aderiu_siasg')->nullable();
            $table->integer('codigo_municipio_siasg')->nullable();
            $table->boolean('situacao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unidades', function (Blueprint $table) {
            $table->dropColumn('sisg');
            $table->dropColumn('aderiu_siasg');
            $table->dropColumn('codigo_municipio_siasg');
            $table->dropColumn('situacao');
        });
    }
}
