<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddUniquePlanointerno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $constraintExists = DB::select(DB::raw("
            SELECT COUNT(*)
            FROM information_schema.table_constraints
            WHERE table_name = 'planointerno'
            AND constraint_type = 'UNIQUE'
            AND constraint_name = 'pi_unique'
        "));
        if ($constraintExists[0]->count == 0) {
            Schema::table('planointerno', function (Blueprint $table) {
                $table->unique(['codigo', 'orgao', 'ano'], 'pi_unique');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planointerno', function (Blueprint $table) {
            $table->dropUnique('pi_unique');
        });
    }
}
