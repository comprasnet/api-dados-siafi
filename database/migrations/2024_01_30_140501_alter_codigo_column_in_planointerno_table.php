<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCodigoColumnInPlanointernoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planointerno', function (Blueprint $table) {
                $table->dropUnique('planointerno_codigo_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planointerno', function (Blueprint $table) {
            // Here you would define how to rollback the changes, if needed
        });
    }
}
