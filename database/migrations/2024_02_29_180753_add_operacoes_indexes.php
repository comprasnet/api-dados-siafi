<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOperacoesIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('operacoes', function (Blueprint $table) {
            $schemaManager = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound  = $schemaManager->listTableIndexes('operacoes');
            // Index "empenhos_ano_idx"
            if (!array_key_exists("idx_operacoes_nome", $indexesFound)) {
                $table->index('nome_operacao', 'idx_operacoes_nome');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operacoes', function (Blueprint $table) {
            $table->dropIndex('idx_operacoes_nome');
        });
    }
}
