<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCollumnSistemaorigemEmpenhodetalhadoEmpenhos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empenhodetalhado', function (Blueprint $table) {
            $table->string('sistemaorigem')->nullable()->change();
        });

        Schema::table('empenhos', function (Blueprint $table) {
            $table->string('sistemaorigem')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhodetalhado', function (Blueprint $table) {
            $table->timestamp('sistemaorigem')->nullable()->change();
        });

        Schema::table('empenhos', function (Blueprint $table) {
            $table->timestamp('sistemaorigem')->nullable()->change();
        });
    }
}
