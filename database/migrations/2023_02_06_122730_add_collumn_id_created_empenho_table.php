<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnIdCreatedEmpenhoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empenhos', function (Blueprint $table) {
            if (!Schema::hasColumn('empenhos', 'id')) {
                $table->bigIncrements('id');
            }
            if (!Schema::hasColumn('empenhos', 'created_at')) {
                $table->timestamp('created_at')->nullable();
            }
            if (!Schema::hasColumn('empenhos', 'ano_emissao')) {
                $table->integer('ano_emissao');
            }
            if (!Schema::hasColumn('empenhos', 'informacaocomplementar')) {
                $table->string('informacaocomplementar', 100)->nullable();
            }
            if (!Schema::hasColumn('empenhos', 'sistemaorigem')) {
                $table->timestamp('sistemaorigem', 100)->nullable();
            }
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhos', function ($table) {
            if (Schema::hasColumn('empenhos', 'id')) {
                $table->dropColumn('id');
            }
            if (Schema::hasColumn('empenhos', 'created_at')) {
                $table->dropColumn('created_at');
            }
            if (Schema::hasColumn('empenhos', 'ano_emissao')) {
                $table->dropColumn('ano_emissao');
            }
            if (Schema::hasColumn('empenhos', 'informacaocomplementar')) {
                $table->dropColumn('informacaocomplementar');
            }
            if (Schema::hasColumn('empenhos', 'sistemaorigem')) {
                $table->dropColumn('sistemaorigem');
            }
        });
    }
}