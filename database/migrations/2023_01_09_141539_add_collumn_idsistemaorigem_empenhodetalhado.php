<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnIdsistemaorigemEmpenhodetalhado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('empenhodetalhado', 'id_sistema_origem')){
            Schema::table('empenhodetalhado', function (Blueprint $table) {
                $table->string('id_sistema_origem')->default("SIAFI");
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhodetalhado', function ($table) {
            $table->dropColumn('id_sistema_origem');
        });
    }
}
