<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnIdCreatedEmpenhodetalhadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empenhodetalhado', function (Blueprint $table) {
            if (!Schema::hasColumn('empenhodetalhado', 'id')) {
                $table->bigIncrements('id');
            }
            if (!Schema::hasColumn('empenhodetalhado', 'created_at')) {
                $table->timestamp('created_at')->nullable();
            }
            if (!Schema::hasColumn('empenhodetalhado', 'ano_emissao')) {
                $table->integer('ano_emissao');
            }
            if (!Schema::hasColumn('empenhodetalhado', 'sistemaorigem')) {
                $table->timestamp('sistemaorigem', 100)->nullable();
            }
            if (!Schema::hasColumn('empenhodetalhado', 'informacaocomplementar')) {
                $table->string('informacaocomplementar', 100)->nullable();
            }
            if (!Schema::hasColumn('empenhodetalhado', 'operacao')) {
                $table->string('operacao', 50)->nullable();
            }
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhodetalhado', function ($table) {
            if (Schema::hasColumn('empenhodetalhado', 'id')) {
                $table->dropColumn('id');
            }
            if (Schema::hasColumn('empenhodetalhado', 'created_at')) {
                $table->dropColumn('created_at');
            }
            if (Schema::hasColumn('empenhodetalhado', 'ano_emissao')) {
                $table->dropColumn('ano_emissao');
            }
            if (Schema::hasColumn('empenhodetalhado', 'sistemaorigem')) {
                $table->dropColumn('sistemaorigem');
            }
            if (Schema::hasColumn('empenhodetalhado', 'informacaocomplementar')) {
                $table->dropColumn('informacaocomplementar');
            }
            if (!Schema::hasColumn('empenhodetalhado', 'operacao')) {
                $table->dropColumn('operacao');
            }
        });
    }
}