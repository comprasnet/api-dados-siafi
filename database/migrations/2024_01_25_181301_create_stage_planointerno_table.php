<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStagePlanointernoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('stage_planointerno')) {
            Schema::create('stage_planointerno', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string("codigo", 255);
                $table->string("descricao", 255);
                $table->integer("orgao")->nullable();
                $table->integer("ano")->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stage_planointerno');
    }
}
