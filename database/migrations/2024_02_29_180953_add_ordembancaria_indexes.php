<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrdembancariaIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordembancaria', function (Blueprint $table) {
            $schemaManager = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound  = $schemaManager->listTableIndexes('ordembancaria');
            // Index "empenhos_ano_idx"
            if (!array_key_exists("favorecido_index", $indexesFound)) {
                $table->index('favorecido', 'favorecido_index');
            }
            // Index "empenhos_sistemaorigem_idx"
            if (!array_key_exists("ob_index", $indexesFound)) {
                $table->index(['ug', 'gestao', 'numero'], 'ob_index');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordembancaria', function (Blueprint $table) {
            $table->dropIndex('favorecido_index');
            $table->dropIndex('ob_index');
        });
    }
}
