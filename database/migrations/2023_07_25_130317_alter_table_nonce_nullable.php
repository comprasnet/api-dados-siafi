<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNonceNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nonce', function (Blueprint $table) {
            $table->unsignedBigInteger('nonceable_id')->nullable()->change();
            $table->string('nonceable_type')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nonce', function (Blueprint $table) {
            $table->unsignedBigInteger('nonceable_id')->nullable(false)->change();
            $table->string('nonceable_type')->nullable(false)->change();
        });
    }
}
