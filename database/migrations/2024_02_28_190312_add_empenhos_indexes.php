<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddEmpenhosIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empenhos', function (Blueprint $table) {
            $schemaManager = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound  = $schemaManager->listTableIndexes('empenhos');
            // Index "empenhos_ano_idx"
            if (!array_key_exists("empenhos_ano_idx", $indexesFound)) {
                $table->index('ano_emissao', 'empenhos_ano_idx');
            }
            // Index "empenhos_sistemaorigem_idx"
            if (!array_key_exists("empenhos_sistemaorigem_idx", $indexesFound)) {
                $table->index('sistemaorigem', 'empenhos_sistemaorigem_idx');
            }
            // Index "empenhos_ug_idx"
            if (!array_key_exists("empenhos_ug_idx", $indexesFound)) {
                $table->index('ug', 'empenhos_ug_idx');
            }
            // Index "empenhos_ug_updated_anoemissao_idx"
            if (!array_key_exists("empenhos_ug_updated_anoemissao_idx", $indexesFound)) {
                $table->index(['ug', 'gestao', 'updated_at'], 'empenhos_ug_updated_anoemissao_idx');
            }
            // Index "empenhos_ug_updated_anoemissao_leftnumero_idx"
            if (!array_key_exists("empenhos_ug_updated_anoemissao_leftnumero_idx", $indexesFound)) {
                $table->index(['ug', 'updated_at', 'ano_emissao', DB::raw('left(numero::text, 4)')],
                    'empenhos_ug_updated_anoemissao_leftnumero_idx');
            }
            // Index "empenhos_updated_at_idx"
            if (!array_key_exists("empenhos_updated_at_idx", $indexesFound)) {
                $table->index('updated_at', 'empenhos_updated_at_idx')
                    ->orderBy('updated_at', 'desc');
            }
            // Index "idx_empenhos"
            if (!array_key_exists("idx_empenhos", $indexesFound)) {
                $table->index(['num_lista', 'ano_emissao'], 'idx_empenhos');
            }
            // Index "idx_empenhos_001"
            if (!array_key_exists("idx_empenhos_001", $indexesFound)) {
                $table->index(['ug', 'gestao', 'numero'], 'idx_empenhos_001');
            }
            // Index "idx_empenhos_naturezadespesa"
            if (!array_key_exists("idx_empenhos_naturezadespesa", $indexesFound)) {
                $table->index('naturezadespesa', 'idx_empenhos_naturezadespesa');
            }
            // Index "idx_empenhos_num_lista"
            if (!array_key_exists("idx_empenhos_num_lista", $indexesFound)) {
                $table->index('num_lista', 'idx_empenhos_num_lista');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhos', function (Blueprint $table) {
            $table->dropIndex('empenhos_ano_idx');
            $table->dropIndex('empenhos_sistemaorigem_idx');
            $table->dropIndex('empenhos_ug_idx');
            $table->dropIndex('empenhos_ug_updated_anoemissao_idx');
            $table->dropIndex('empenhos_ug_updated_anoemissao_leftnumero_idx');
            $table->dropIndex('empenhos_updated_at_idx');
            $table->dropIndex('idx_empenhos');
            $table->dropIndex('idx_empenhos_001');
            $table->dropIndex('idx_empenhos_naturezadespesa');
            $table->dropIndex('idx_empenhos_num_lista');
        });
    }
}
