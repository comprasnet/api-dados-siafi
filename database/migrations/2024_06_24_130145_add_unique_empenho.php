<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddUniqueEmpenho extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $constraintExists = DB::select(DB::raw("
            SELECT COUNT(*)
            FROM information_schema.table_constraints
            WHERE table_name = 'empenhos'
            AND constraint_type = 'UNIQUE'
            AND constraint_name = 'empenhos_unique'
        "));

        if ($constraintExists[0]->count == 0) {
            Schema::table('empenhos', function (Blueprint $table) {
                $table->unique(['ug', 'gestao', 'numero'], 'empenhos_unique');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhos', function (Blueprint $table) {
            $table->dropUnique('empenhos_unique');
        });
    }
}
