<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageOrgaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stage_orgao', function (Blueprint $table) {
            $table->string('codigo')->nullable();
            $table->string('nome')->nullable();
            $table->string('cnpj')->nullable();
            $table->string('orgao_superior')->nullable();
            $table->string('tipo_administracao')->nullable();
            $table->string('poder')->nullable();
            $table->string('esfera')->nullable();
            $table->boolean('situacao')->nullable();
            $table->string('codigosiasg')->nullable();
            $table->string('id_sistema_origem')->nullable();
            $table->boolean('daas')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stage_orgao', function (Blueprint $table) {
            Schema::dropIfExists('stage_orgao');
        });
    }
}
