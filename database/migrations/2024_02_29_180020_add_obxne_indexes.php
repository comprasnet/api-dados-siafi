<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddObxneIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('obxne', static function (Blueprint $table) {
            $schemaManager = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound  = $schemaManager->listTableIndexes('obxne');
            if (!array_key_exists("obxne_index", $indexesFound)) {
                $table->index(['ordembancaria_id','numeroempenho'], 'obxne_index');
            }
            if (!array_key_exists("obxne_index_numeroempenho", $indexesFound)) {
                $table->index('numeroempenho', 'obxne_index_numeroempenho');
            }
            if (!array_key_exists("obxne_index_ordembancaria_id", $indexesFound)) {
                $table->index('ordembancaria_id', 'obxne_index_ordembancaria_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('obxne', function (Blueprint $table) {
            $table->dropIndex('obxne_index');
            $table->dropIndex('obxne_index_numeroempenho');
            $table->dropIndex('obxne_index_ordembancaria_id');
        });
    }
}
