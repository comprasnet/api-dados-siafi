<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnExercicioEmpenhoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empenhos', function (Blueprint $table) {
            if (!Schema::hasColumn('empenhos', 'exercicio_empenho')) {
                $table->smallInteger('exercicio_empenho')->nullable();
            }
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhos', function ($table) {
            if (Schema::hasColumn('empenhos', 'exercicio_empenho')) {
                $table->dropColumn('exercicio_empenho');
            }
        });
    }
}