<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCreatedAtAndUpdatedAtPlanointernoTable extends Migration
{
    public function up(): void
    {
        if (!Schema::hasColumn('planointerno', 'created_at')) {
            Schema::table('planointerno', function (Blueprint $table) {
                $table->timestamp('created_at')->nullable();
            });
        }

        if (!Schema::hasColumn('planointerno', 'updated_at')) {
            Schema::table('planointerno', function (Blueprint $table) {
                $table->timestamp('updated_at')->nullable();
            });
        }

        if (!Schema::hasColumn('planointerno', 'deleted_at')) {
            Schema::table('planointerno', function (Blueprint $table) {
                $table->timestamp('deleted_at')->nullable();
            });
        }
    }

    public function down(): void
    {
        Schema::table('planointerno', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
        });
    }
}
