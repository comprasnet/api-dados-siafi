<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnContratoIdEmpenhosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('empenhos', 'contrato_id')) {
            Schema::table('empenhos', function (Blueprint $table) {
                $table->integer('contrato_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhos', function ($table) {
            if (Schema::hasColumn('empenhos', 'contrato_id')) {
                $table->dropColumn('contrato_id');
            }
        });
    }
}
