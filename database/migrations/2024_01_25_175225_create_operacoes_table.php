<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('operacoes')) {
            Schema::create('operacoes', function (Blueprint $table) {
                $table->bigIncrements('id_operacao');
                $table->string("nome_operacao");
                $table->integer("mult_quantidade")->nullable();
                $table->integer("mult_valortotal")->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operacoes');
    }
}
