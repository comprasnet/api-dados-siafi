<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpenhodetalhadoIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empenhodetalhado', static function (Blueprint $table) {
            $schemaManager = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound  = $schemaManager->listTableIndexes('empenhodetalhado');
            if (!array_key_exists("empenhodetalhado_anoemissao_idx", $indexesFound)) {
                $table->index('ano_emissao', 'empenhodetalhado_anoemissao_idx');
            }
            if (!array_key_exists("empenhodetalhado_numeroli_idx", $indexesFound)) {
                $table->index('numeroli', 'empenhodetalhado_numeroli_idx');
            }
            if (!array_key_exists("empenhodetalhado_updated_at_idx", $indexesFound)) {
                $table->index('updated_at', 'empenhodetalhado_updated_at_idx')
                    ->orderBy('updated_at', 'desc');
            }
            if (!array_key_exists("idx_empenhos_subitem", $indexesFound)) {
                $table->index('subitem', 'idx_empenhos_subitem');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhodetalhado', function (Blueprint $table) {
            $table->dropIndex('empenhodetalhado_anoemissao_idx');
            $table->dropIndex('empenhodetalhado_numeroli_idx');
            $table->dropIndex('empenhodetalhado_updated_at_idx');
            $table->dropIndex('idx_empenhos_subitem');
        });
    }
}
