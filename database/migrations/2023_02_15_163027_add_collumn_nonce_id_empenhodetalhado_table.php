<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnNonceIdEmpenhodetalhadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empenhodetalhado', function (Blueprint $table) {
            if (!Schema::hasColumn('empenhodetalhado', 'nonce_id')) {
                $table->bigInteger('nonce_id')->nullable();
            }
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhodetalhado', function ($table) {
            if (Schema::hasColumn('empenhodetalhado', 'nonce_id')) {
                $table->dropColumn('nonce_id');
            }
        });
    }
}