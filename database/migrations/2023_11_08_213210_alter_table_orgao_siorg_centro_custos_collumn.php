<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrgaoSiorgCentroCustosCollumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orgao', function (Blueprint $table) {
            if (!Schema::hasColumn('orgao', 'utiliza_centro_custo')) {
                $table->string('utiliza_centro_custo')->nullable();
            }
            if (!Schema::hasColumn('orgao', 'codigo_siorg')) {
                $table->string('codigo_siorg')->nullable();
            }
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orgao', function ($table) {
            if (Schema::hasColumn('orgao', 'utiliza_centro_custo')) {
                $table->dropColumn('utiliza_centro_custo');
            }
            if (Schema::hasColumn('orgao', 'codigo_siorg')) {
                $table->dropColumn('codigo_siorg');
            }
        });
    }
}
