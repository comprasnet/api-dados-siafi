
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnsOrgaoAnoPlanointernoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('planointerno', 'orgao')) {
            Schema::table('planointerno', function (Blueprint $table) {
                $table->integer('orgao')->nullable();
            });
        }

        if (!Schema::hasColumn('planointerno', 'ano')) {
            Schema::table('planointerno', function (Blueprint $table) {
                $table->integer('ano')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhos', function ($table) {
            if (Schema::hasColumn('empenhos', 'orgao')) {
                $table->dropColumn('orgao');
            }
        });

        Schema::table('empenhos', function ($table) {
            if (Schema::hasColumn('empenhos', 'ano')) {
                $table->dropColumn('ano');
            }
        });
    }
}