<?php

return [
    'app_version' => '1.00.00-r0',
    'app_amb' => env('APP_AMB', 'Desenvolvimento'),
    'SICAF_HOST' => env('SICAF_HOST'),
    'SICAF_USUARIO' => env('SICAF_USUARIO'),
    'SICAF_SENHA' => env('SICAF_SENHA'),
    'SICAF_ENV' => env('SICAF_ENV'),

    'path_pendentes' => env('ARQUIVOS_QWARE_RECEBIDOS'),
    'path_processados' => env('ARQUIVOS_QWARE_PROCESSADOS'),

    'edf_reinf_deducoes' => [
        'DDF022',
        'DDF023',
    ],

    'saldo_contas_contabeis' => [
        "531110100",
        "531110200",
        "531210000",
        "531220000",
        "531300000",
        "531610000",
        "531620000",
        "531630000",
        "531640000",
        "532100000",
        "532200000",
        "532600000",
        "622920101",
        "622920102",
        "622920103",
        "622920104",
        "622920105",
        "622920106",
        "622920107",
        "631100000",
        "631200000",
        "631300000",
        "631400000",
        "631510000",
        "631520000",
        "631610000",
        "631620000",
        "631630000",
        "631640000",
        "631910000",
        "631980000",
        "631990000",
        "632100000",
        "632200000",
        "632600000",
        "632910100",
        "632910200",
        "622110000",
        "111122001",
        "891100000",
        "622130300",
        "822130400",
        "822140400",
    ],

    'datas_carga_rps' => [
        '01-01',
        '01-02',
        '01-03',
        '01-04',
        '01-05',
        '01-06',
        '01-07',
        '01-08',
        '01-09',
        '01-10',
        '01-11',
        '01-12',
        '01-13',
        '01-14',
        '01-15',
    ],

    'url_contratos_unidades' => env('URL_CONTRATOS_UNIDADES','https://contratos.comprasnet.gov.br/api/contrato/unidades'),

//    'java_jdbc_command' => '"c:\Program Files\Java\jdk-11.0.7\bin\java.exe"  -Dfile.encoding=UTF-8  -classpath C:\Users\heles.junior\Documents\Projetos\jdbcClient\libs\*;jboss-dv-6.3.0-teiid-jdbc.jar -Djavax.net.ssl.trustStore="C:\Users\heles.junior\Documents\Projetos\jdbcClient\libs\daas.serpro.gov.br.jks"  -DdaasUrl="jdbc:teiid:DAAS_DB_DATABASE1@mms://DAAS_DB_HOST1:DAAS_DB_PORT1" -DdaasUser="DAAS_DB_USERNAME1" -DdaasPwd="DAAS_DB_PASSWORD1" -DdaasColumnDelimiter="DAAS_DB_DELIMITER1"  PARAMETERS JDBC_CLASS.java',
//    'java_jdbc_path' => 'C:\Users\heles.junior\Documents\Projetos\jdbcClient',
    'java_jdbc_path' => '/var/www/jdbcClient',
    'java_jdbc_command' => 'java -Dfile.encoding=UTF-8  -classpath .:./libs/jboss-dv-6.3.0-teiid-jdbc.jar -Djavax.net.ssl.trustStore="./libs/daas.serpro.gov.br.jks"  -DdaasUrl="jdbc:teiid:DAAS_DB_DATABASE1@mms://DAAS_DB_HOST1:DAAS_DB_PORT1" -DdaasUser="DAAS_DB_USERNAME1" -DdaasPwd="DAAS_DB_PASSWORD1" -DdaasColumnDelimiter="DAAS_DB_DELIMITER1" PARAMETERS JDBC_CLASS',


    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Api STA'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services the application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Protocolo HTTP
    |--------------------------------------------------------------------------
    |
    | Definição do protocolo HTTP que deverá ser usado pela aplicação.
    |
    */
    'protocolo_http' => env('PROTOCOLO_HTTP', 'http'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    'asset_url' => env('ASSET_URL', null),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'America/Sao_Paulo',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Faker Locale
    |--------------------------------------------------------------------------
    |
    | This locale will be used by the Faker PHP library when generating fake
    | data for your database seeds. For example, this will be used to get
    | localized telephone numbers, street address information and more.
    |
    */

    'faker_locale' => 'en_US',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        #L5Swagger\L5SwaggerServiceProvider::class,

        /*
         * Package Service Providers...
         */

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Arr' => Illuminate\Support\Arr::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'Str' => Illuminate\Support\Str::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

    ],

];
