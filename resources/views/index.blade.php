<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>API Dados SIAFI - STA</title>
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
  <style>
    body{
      font-family: 'Titillium Web', sans-serif;
      color: #666666;
      text-align: center;
    }
    .box{
      background-color: #fff;
      width: 500px;
      margin: 50px auto;
      display: flex;
      flex-direction: column;
      align-items: center;
    }
    #title {
      font-size: 30px;
      margin-top: 25px;
    }
    #descricao{
      font-size:20px;
      margin: 20px auto;
    }
    #logo {
      margin-top: 25px;
      max-width: 365px;
      height: auto;
    }
    #logo-task {
      width: 100px;
    }
    #logo-task1 {
      width: 100px;
    }
    #logo-task2 {
      max-height: 15px;
      margin-right: 10px;
    }
    a {
      text-decoration: none;
    }
    .footer {
      position: fixed;
      left: 0;
      bottom: 0;
      width: 98%;
      color: #666666;
      text-align: right;
    }
  </style>
</head>
<body>
  <div class="box">
    <a href="https://gitlab.com/comprasnet/api-dados-siafi" target="_blank">
      <img src="{{ env('URL_APP') }}img/logo-api-dados-siafi.png" id="logo">
    </a>
    <div id="title">
      SEGES-DELOG-CGCON-COCTR<br>
      Ministério da Gestão e da Inovação em Serviços Públicos
    </div>
    <div id="descricao">
      Ferramenta de Leitura de arquivos do STA - Sistema de Transferência de Arquivos - STN<br>
      Para disponibilização das informações constantes do SIAFI, por meio de uma API RestFull.   
    </div>
    <div style="display: flex; align-items: center;">
      <a href="{{ env('URL_APP') }}/api/documentation" target="_self">
        <img src="{{ env('URL_APP') }}img/swagger-icon.png" id="logo-task2">
      </a>
      <a href="https://contratos.comprasnet.gov.br/login" target="_blank">
        <img src="{{ env('URL_APP') }}img/logo-contratos.png" id="logo-task1">
      </a>
    </div>
  </div>
  <div class="footer">
    <p>v. {{  config('app.app_version') }}</p>
  </div>
</body>
</html>